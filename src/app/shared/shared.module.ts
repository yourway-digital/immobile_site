import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from '../admin/components/loading/loading.component';
import { MaterialModule } from '../material/material.module';

@NgModule({
  declarations: [LoadingComponent],
  imports: [CommonModule, MaterialModule],
  exports: [LoadingComponent],
})
export class SharedModule {}
