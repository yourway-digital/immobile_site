import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ImEmpreendimentosComponent } from './pages/im-empreendimentos/im-empreendimentos.component';
import { HomeComponent } from './pages/home/home.component';
import { ImEmpreendimentosCriarComponent } from './pages/im-empreendimentos-criar/im-empreendimentos-criar.component';
import { ImEmpreendimentosEditarComponent } from './pages/im-empreendimentos-editar/im-empreendimentos-editar.component';
import { ImUsuariosCriarComponent } from './pages/im-usuarios-criar/im-usuarios-criar.component';
import { ImUsuariosEditarComponent } from './pages/im-usuarios-editar/im-usuarios-editar.component';
import { ImUsuariosComponent } from './pages/im-usuarios/im-usuarios.component';
import { PerfilComponent } from './pages/perfil/perfil.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'empreendimentos', component: ImEmpreendimentosComponent },
  { path: 'empreendimentos/adicionar', component: ImEmpreendimentosCriarComponent },
  { path: 'empreendimentos/editar/:id', component: ImEmpreendimentosEditarComponent },
  { path: 'usuarios', component: ImUsuariosComponent },
  { path: 'usuarios/adicionar', component: ImUsuariosCriarComponent },
  { path: 'usuarios/editar/:id', component: ImUsuariosEditarComponent },
  { path: 'perfil', component: PerfilComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImobiliariaRoutingModule { }
