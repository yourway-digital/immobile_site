import { EmpreendimentoEstatistica } from 'src/app/models/empreendimento-estatistica.model';
import { EstatisticaService } from 'src/app/services/admin/estatistica.service';
// default
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { merge, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-im-estatisticas',
  templateUrl: './im-estatisticas.component.html',
  styleUrls: ['./im-estatisticas.component.scss']
})
export class ImEstatisticasComponent implements OnInit {

  @Input('idEmpreendimento') idEmpreendimento: number;

  displayedColumns: string[] = ['1', '2', '3', '4', '5', 'actions'];
  // dataHora, usuarioApp.nome, usuarioApp.email, usuarioApp.celular, tipo, 
  data: EmpreendimentoEstatistica[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  search: string;
  filterSubscription: Subscription;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  situacao: string = 'A';

  tipo: string = '';

  constructor(
    public global: GlobalService,
    public estatisticaService: EstatisticaService,
    public dialog: MatDialog,
    public helper: HelperService,
    public loadingService: LoadingService
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.buscar();
  }

  buscar() {
    merge(this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.estatisticaService.getData(this.paginator.pageIndex + 1, this.paginator.pageSize, this.tipo, this.idEmpreendimento, null, null, null);
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.length * this.paginator.pageSize;
          return data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.data = data;
      });
  }

}
