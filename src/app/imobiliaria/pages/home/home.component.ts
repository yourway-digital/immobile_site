import { Component, HostListener, OnInit } from '@angular/core';
import { Empreendimento } from 'src/app/models/empreendimento.model';
import { TipoEmpreendimento } from 'src/app/models/tipo-empreendimento.model';
import { UsuarioImobiliaria } from 'src/app/models/usuario-imobiliaria.model';
import { EmpreendimentoService } from 'src/app/services/admin/empreendimento.service';
import { EstatisticaService } from 'src/app/services/admin/estatistica.service';
import { ImobiliariaService } from 'src/app/services/admin/imobiliaria.service';
import { TipoEmpreendimentoService } from 'src/app/services/admin/tipo-empreendimento.service';
import { UsuarioImobiliariaService } from 'src/app/services/admin/usuario-imobiliaria.service';
import { AuthService } from 'src/app/services/auth.service';
import {
  empreendimentoTiposOferta,
  GlobalService,
} from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  empreendimentos: Empreendimento[] = [];

  idUsuarioImobiliaria = 0;
  searchUsuariosImobiliaria = '';
  usuariosImobiliaria: UsuarioImobiliaria[] = [];

  tiposEmpreendimento: TipoEmpreendimento[] = [];
  searchTiposEmpreendimento = '';

  page = 1;
  noMore: boolean = false;
  itemsPerPage = 8;
  sortBy = '1';
  ordination = 'ASC';
  idTipoEmpreendimento: any = '';
  search = '';
  situacao = 'A';
  status = 'A';
  tipoOferta = '';
  dataInicial: Date = new Date();
  dataFinal: Date = new Date();

  loading: boolean = false;

  homeStats: any = {};

  empreendimentoTiposOferta = empreendimentoTiposOferta;

  @HostListener('window:scroll', [])
  handleScroll(): void {
    if (
      window.innerHeight + window.scrollY >=
      document.body.offsetHeight - 44
    ) {
      this.loadMore();
    }
  }

  constructor(
    public _auth: AuthService,
    public empreendimentoService: EmpreendimentoService,
    public tipoEmpreendimentoService: TipoEmpreendimentoService,
    public usuarioImobiliariaService: UsuarioImobiliariaService,
    public estatisticaService: EstatisticaService,
    public global: GlobalService,
    private imobiliariaService: ImobiliariaService,
    private loadingService: LoadingService,
    private helper: HelperService
  ) {
    this.dataInicial = this.helper.moment().subtract('30', 'days').toDate();
    if (this._auth.user.tipo === 'C') {
      this.idUsuarioImobiliaria = this._auth.user.id;
    }
  }

  ngOnInit(): void {
    // this.buscar();
    this.buscarTiposEmpreendimentos();
    this.buscarCreditos();
    this.buscarUsuariosImobiliaria();
    this.buscarDadosContadores();
  }

  buscar() {
    if (this.noMore) return;

    this.loading = true;

    this.empreendimentoService
      .get(
        this.page,
        this.itemsPerPage,
        this.sortBy,
        this.ordination,
        this._auth.user.imobiliaria.id,
        this.status ? this.status : '%',
        this.tipoOferta ? this.tipoOferta : '%',
        this.idTipoEmpreendimento ? this.idTipoEmpreendimento : 'null',
        this.idUsuarioImobiliaria ? this.idUsuarioImobiliaria : '%',
        this.situacao ? this.situacao : '%',
        this.search,
        true
      )
      .subscribe(
        (res) => {
          if (res.empreendimentos.length < this.itemsPerPage) {
            this.noMore = true;
          }
          this.empreendimentos =
            this.page == 1
              ? res.empreendimentos
              : this.empreendimentos.concat(res.empreendimentos);
          this.loading = false;
        },
        (e) => (this.loading = false)
      );
  }

  buscarTiposEmpreendimentos() {
    this.tipoEmpreendimentoService
      .get(-99, -99, '1', 'ASC', 'A')
      .subscribe((res) => (this.tiposEmpreendimento = res.tiposEmpreendimento));
  }

  buscarCreditos() {
    this.usuarioImobiliariaService
      .hasCredit(this._auth.user.id)
      .subscribe((res) => {
        console.log(res);
      });
  }

  buscarUsuariosImobiliaria() {
    this.usuarioImobiliariaService
      .get(-99, -99, '1', 'ASC', this._auth.user.imobiliaria.id, '%', 'A')
      .subscribe((res) => (this.usuariosImobiliaria = res.usuarios));
  }

  buscarDadosContadores() {
    this.estatisticaService
      .getAccountants(
        this._auth.user.imobiliaria.id,
        this._auth.user.tipo === 'C' ? this._auth.user.id : null,
        this.helper.moment(this.dataInicial).format('YYYY-MM-DD'),
        this.helper.moment(this.dataFinal).format('YYYY-MM-DD')
      )
      .subscribe((res) => (this.homeStats = res));
  }

  onTipoOfertaChange(e) {
    this.noMore = false;
    this.page = 1;
    this.buscar();
  }

  onUsuarioImobiliariaChange(e) {
    this.noMore = false;
    this.page = 1;
    this.buscar();
  }

  onImobiliariaChange(e) {
    this.noMore = false;
    this.page = 1;
    this.buscar();
  }

  onTipoEmpreendimentoChange(e) {
    this.noMore = false;
    this.page = 1;
    this.buscar();
  }

  loadMore() {
    if (this.noMore || this.loading) return;
    this.page++;
    this.buscar();
  }

  onDataFinalChange(ev) {
    if (!this.dataFinal || !this.dataInicial) return;

    this.noMore = false;
    this.page = 1;
    this.buscarDadosContadores();
  }

  generateExcel() {
    this.loadingService.present('Gerando excel...');
    this.imobiliariaService
      .generateExcel(
        this._auth.user.imobiliaria.id,
        this.dataInicial,
        this.dataFinal
      )
      .subscribe(
        (res) => {
          if (res) {
            window.open(res, '_blank');
          }
          this.loadingService.dismiss();
        },
        (e) => this.loadingService.dismiss()
      );
  }
}
