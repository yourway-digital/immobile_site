import { Empreendimento } from 'src/app/models/empreendimento.model';
import {
  EmpreendimentosApi,
  EmpreendimentoService,
} from 'src/app/services/admin/empreendimento.service';
// default
import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { DialogComponent } from 'src/app/admin/components/dialog/dialog.component';
import {
  empreendimentoStatus,
  empreendimentoTiposOferta,
  GlobalService,
} from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ImobiliariaService } from 'src/app/services/admin/imobiliaria.service';
import { TipoEmpreendimentoService } from 'src/app/services/admin/tipo-empreendimento.service';
import { TipoEmpreendimento } from 'src/app/models/tipo-empreendimento.model';
import { AuthService } from 'src/app/services/auth.service';
import { UsuarioImobiliaria } from 'src/app/models/usuario-imobiliaria.model';
import { UsuarioImobiliariaService } from 'src/app/services/admin/usuario-imobiliaria.service';

@Component({
  selector: 'app-im-empreendimentos',
  templateUrl: './im-empreendimentos.component.html',
  styleUrls: ['./im-empreendimentos.component.scss'],
})
export class ImEmpreendimentosComponent implements OnInit {
  displayedColumns: string[] = [
    'select',
    'imagem',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    'actions',
  ];
  // imagem, nome, endereco, valor, imobiliaria, tipoEmpreendimento, status, tipo da oferta, situacao
  data: Empreendimento[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  idTipoEmpreendimento: number = 0;
  searchTiposEmpreendimento: string = '';
  tiposEmpreendimento: TipoEmpreendimento[] = [];

  idUsuarioImobiliaria: number = 0;
  searchUsuariosImobiliaria: string = '';
  usuariosImobiliaria: UsuarioImobiliaria[] = [];

  status: string = '';
  tipoOferta: string = '';
  situacao: string = '';
  search: string = '';

  filterSubscription: Subscription;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  selection = new SelectionModel<Empreendimento>(true, []);

  empreendimentoStatus = empreendimentoStatus;
  empreendimentoTiposOferta = empreendimentoTiposOferta;

  constructor(
    public global: GlobalService,
    public empreendimentoService: EmpreendimentoService,
    public imobiliariaService: ImobiliariaService,
    public tipoEmpreendimentoService: TipoEmpreendimentoService,
    public usuarioImobiliariaService: UsuarioImobiliariaService,
    public dialog: MatDialog,
    public helper: HelperService,
    public loadingService: LoadingService,
    public auth: AuthService
  ) {
    if (this.auth.user.tipo === 'C') {
      this.idUsuarioImobiliaria = this.auth.user.id;
    }
  }

  ngOnInit() {
    this.buscarTiposEmpreendimentos();
    this.buscarUsuariosImobiliaria();
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    this.buscar();
  }

  buscarTiposEmpreendimentos() {
    this.tipoEmpreendimentoService
      .get(-99, -99, '1', 'ASC', 'A')
      .subscribe((res) => (this.tiposEmpreendimento = res.tiposEmpreendimento));
  }

  buscarUsuariosImobiliaria() {
    this.usuarioImobiliariaService
      .get(-99, -99, '1', 'ASC', this.auth.user.imobiliaria.id, '%', 'A')
      .subscribe((res) => (this.usuariosImobiliaria = res.usuarios));
  }

  buscar() {
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.empreendimentoService.get(
            this.paginator.pageIndex + 1,
            this.paginator.pageSize,
            this.sort.active,
            this.sort.direction.toLocaleUpperCase(),
            this.auth.user.imobiliaria.id,
            this.status,
            this.tipoOferta,
            this.idTipoEmpreendimento,
            this.idUsuarioImobiliaria ? this.idUsuarioImobiliaria : '%',
            this.situacao,
            this.search, 
            false
          );
        }),
        map((data) => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.selection.clear();
          this.resultsLength = data.numeroPaginas * this.paginator.pageSize;
          return data.empreendimentos;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          this.selection.clear();
          return observableOf([]);
        })
      )
      .subscribe((data) => {
        this.data = data;
      });
  }

  deletarData(item: Empreendimento) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {
        title: 'Excluir item',
        description:
          'Você realmente quer excluir esse item? Esse processe não pode ser desfeito.',
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.loadingService.present('Excluindo item...');
        this.empreendimentoService.delete(item).subscribe(
          (res) => {
            this.helper.openSnackBar('Remoção', 'Item removido com sucesso.');
            this.loadingService.dismiss();
            this.buscar();
          },
          (e) => this.loadingService.dismiss()
        );
      }
    });
  }

  deletarDatas() {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {
        title: 'Excluir itens',
        description:
          'Você realmente quer excluir esses itens? Esse processe não pode ser desfeito.',
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.empreendimentoService
          .deleteSelected(this.selection.selected)
          .subscribe(
            (res: any) => {
              this.loadingService.present('Excluindo itens');
              this.helper.openSnackBar(
                'Remoção',
                'Itens removidos com sucesso.'
              );
              this.loadingService.dismiss();
              this.buscar();
            },
            (e) => this.loadingService.dismiss()
          );
      }
    });
  }

  filter(e) {
    if (this.paginator.pageIndex > 1) {
      this.paginator.pageIndex = 0;
    }
    if (e) {
      if (this.filterSubscription && !this.filterSubscription.closed) {
        this.filterSubscription.unsubscribe();
      }
      this.filterSubscription = this.empreendimentoService
        .get(
          this.paginator.pageIndex + 1,
          this.paginator.pageSize,
          this.sort.active,
          this.sort.direction.toLocaleUpperCase(),
          this.auth.user.imobiliaria.id,
          this.status,
          this.tipoOferta,
          this.idTipoEmpreendimento,
          this.idUsuarioImobiliaria ? this.idUsuarioImobiliaria : '%',
          this.situacao,
          this.search, 
          false
        )
        .subscribe((res: EmpreendimentosApi) => {
          this.data =
            this.paginator.pageIndex == 0
              ? res.empreendimentos
              : this.data.concat(res.empreendimentos);
        });
    } else {
      this.buscar();
    }
  }

  clearFilter() {
    this.search = '';
    this.buscar();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.data.forEach((row) => this.selection.select(row));
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.position + 1
    }`;
  }

  onStatusChange(e) {
    this.buscar();
  }

  onTipoOfertaChange(e) {
    this.buscar();
  }

  onSituacaoChange(e) {
    this.buscar();
  }

  onTipoEmpreendimentoChange(e) {
    this.buscar();
  }

  onUsuarioImobiliariaChange(e) {
    this.buscar();
  }
}
