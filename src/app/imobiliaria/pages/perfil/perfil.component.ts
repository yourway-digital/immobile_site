import { Imobiliaria } from 'src/app/models/imobiliaria.model';
import { ImobiliariaService } from 'src/app/services/admin/imobiliaria.service';
// Default
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioImobiliariaService } from 'src/app/services/admin/usuario-imobiliaria.service';
import { CreditsDialogComponent } from 'src/app/admin/components/credits-dialog/credits-dialog.component';
import { UploadDialogComponent } from 'src/app/admin/components/upload-dialog/upload-dialog.component';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {

  data: Imobiliaria = new Imobiliaria();

  phoneMask = '(00) 0000-00009';
  whatsappMask = '(00) 0000-00009';

  constructor(
    public imobiliariaService: ImobiliariaService,
    public usuarioImobiliariaService: UsuarioImobiliariaService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public route: ActivatedRoute,
    public global: GlobalService,
    public dialog: MatDialog,
    public auth: AuthService
  ) {
    if (this.auth.user.tipo !== 'A') {
      this.router.navigate(['/imobiliaria'])
      return;
    }
  }

  ngOnInit() {
    this.route.params.subscribe(params => this.buscar(params?.['id']));
  }

  buscar(id) {
    this.imobiliariaService.getById(this.auth.user.imobiliaria.id)
      .subscribe(res => this.data = res);
  }

  submit(f: NgForm) {

    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar('Formulário inválido', 'Preencha os campos requiridos, por favor!', true);
      return;
    }

    this.loadingService.present('Alterando item...');

    this.imobiliariaService.patch(this.data)
      .subscribe(res => {
        this.router.navigate(['/admin/imobiliarias']).then(() => {
          this.helper.openSnackBar('Alteração', 'Item alterado com sucesso.');
          this.loadingService.dismiss();
        })
      }, e => this.loadingService.dismiss());
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? this.data.situacao = 'A' : this.data.situacao = 'I';
  }

  openUploadDialog() {
    const dialogRef = this.dialog.open(UploadDialogComponent, {
      width: '640px',
      data: {
        imagePath: this.data.imagem,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) return;
      this.data.imagem = result;
    });
  }

  changePhoneMask(event, field) {
    let val = event;
    console.log(this[field]);
    if (!val) {
      return;
    }
    if (val.replace(/\D/g, '').substring(0, 4) == "0800") {
      this[field] = '0000 000 0000';
    } else {
      if (val.replace(/\D/g, '').length === 11) {
        this[field] = '(00) 0 0000-0000';
      } else {
        this[field] = '(00) 0000-00009';
      }
    }
  }

  @ViewChild('enderecoNumeroInput', { static: false }) enderecoNumeroInput: ElementRef;
  onCepBlur(e) {
    if (this.data.cep.length == 8) {
      this.helper.viaCep(this.data.cep)
        .subscribe((res) => {
          this.data.bairro = res.bairro;
          this.data.uf = res.uf;
          this.data.cidade = res.localidade;
          this.data.endereco = res.logradouro;
          this.enderecoNumeroInput.nativeElement.focus();
        });
    }
  }

  openCreditsDialog() {
    const dialogRef = this.dialog.open(CreditsDialogComponent, {
      width: '800px',
      data: this.data,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.buscar(this.data.id);
      }
    });
  }

}
