import { UsuarioImobiliaria } from 'src/app/models/usuario-imobiliaria.model';
import { UsuarioImobiliariaService } from 'src/app/services/admin/usuario-imobiliaria.service';
// default
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogPasswordComponent } from 'src/app/admin/components/dialog-password/dialog-password.component';

@Component({
  selector: 'app-im-usuarios-editar',
  templateUrl: './im-usuarios-editar.component.html',
  styleUrls: ['./im-usuarios-editar.component.scss'],
})
export class ImUsuariosEditarComponent implements OnInit {
  data: UsuarioImobiliaria = new UsuarioImobiliaria();

  searchImobiliarias: string = '';
  showPassword: boolean = false;

  phoneMask: string = '(00) 0000-00009';

  constructor(
    public usuarioImobiliariaService: UsuarioImobiliariaService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public route: ActivatedRoute,
    public global: GlobalService,
    public auth: AuthService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      console.log(this.auth.user, params?.['id']);
      if (this.auth.user.tipo === 'C' && this.auth.user.id != params?.['id']) {
        this.router.navigate(['/imobiliaria']);
        return;
      }

      this.buscar(params?.['id']);
    });
  }

  buscar(id) {
    this.usuarioImobiliariaService.getById(id).subscribe((res) => {
      this.data = res;
      this.data.senha = atob(res.senha);
    });
  }

  submit(f: NgForm) {
    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar(
        'Formulário inválido',
        'Preencha os campos requiridos, por favor!',
        true
      );
      return;
    }

    this.loadingService.present('Alterando item...');

    this.usuarioImobiliariaService.patch(this.data).subscribe(
      (res: any) => {
        this.router.navigate(['/imobiliaria/usuarios']).then(() => {
          this.helper.openSnackBar('Alteração', 'Item alterado com sucesso.');
          this.loadingService.dismiss();
        });
      },
      (e) => this.loadingService.dismiss()
    );
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? (this.data.situacao = 'A') : (this.data.situacao = 'I');
  }

  openPasswordDialog() {
    const dialogRef = this.dialog.open(DialogPasswordComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        console.log(result);
        this.loadingService.present('Alterando senha...');
        this.usuarioImobiliariaService
          .changePassword(this.data.email, result)
          .subscribe(
            (res) => {
              this.helper.openSnackBar(
                'Senha alterada com sucesso.',
                'Alterar senha'
              );
              this.loadingService.dismiss();
            },
            (e) => this.loadingService.dismiss()
          );
      }
    });
  }

  changePhoneMask(event) {
    let val = event;
    if (!val) {
      return;
    }
    if (val.replace(/\D/g, '').substring(0, 4) == '0800') {
      this.phoneMask = '0000 000 0000';
    } else {
      if (val.replace(/\D/g, '').length === 11) {
        this.phoneMask = '(00) 0 0000-0000';
      } else {
        this.phoneMask = '(00) 0000-00009';
      }
    }
  }
}
