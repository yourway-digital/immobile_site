import { UsuarioImobiliaria } from 'src/app/models/usuario-imobiliaria.model';
import { UsuarioImobiliariaService } from 'src/app/services/admin/usuario-imobiliaria.service';
// default
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-im-usuarios-criar',
  templateUrl: './im-usuarios-criar.component.html',
  styleUrls: ['./im-usuarios-criar.component.scss']
})
export class ImUsuariosCriarComponent implements OnInit {

  data: UsuarioImobiliaria = new UsuarioImobiliaria();

  searchImobiliarias: string = '';

  constructor(
    public usuarioImobiliariaService: UsuarioImobiliariaService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public global: GlobalService,
    public auth: AuthService,
  ) {
    if (this.auth.user.tipo !== 'A') {
      this.router.navigate(['/imobiliaria'])
      return;
    }
  }

  ngOnInit() {
  }

  submit(f: NgForm) {

    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar('Formulário inválido', 'Preencha os campos requiridos, por favor!', true);
      return;
    }

    if (this.data.senha !== this.data.re_senha) {
      this.helper.openSnackBar('Inserir usuário', 'As senhas precisam ser iguais.', true);
      return;
    }

    this.loadingService.present('Inserindo item...');

    const user = {
      ...this.data,
      imobiliaria: this.auth.user.imobiliaria
    } as UsuarioImobiliaria;
    delete user['re_senha'];

    this.usuarioImobiliariaService.post(user)
      .subscribe((res: any) => {
        this.router.navigate(['/imobiliaria/usuarios']).then(() => {
          this.helper.openSnackBar('Inserção', 'Item inserido com sucesso.');
          this.loadingService.dismiss();
        })
      }, e => this.loadingService.dismiss());
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? this.data.situacao = 'A' : this.data.situacao = 'I';
  }

}
