import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { HttpProgressEvent } from '@angular/common/http';
import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { MatStepper } from '@angular/material/stepper';
import { Router } from '@angular/router';
import { AddressDialogComponent } from 'src/app/admin/components/address-dialog/address-dialog.component';
import { Empreendimento } from 'src/app/models/empreendimento.model';
import { Imobiliaria } from 'src/app/models/imobiliaria.model';
import { Endereco } from 'src/app/models/endereco.model';
import { SubtipoEmpreendimento } from 'src/app/models/subtipo-empreendedorismo.model';
import { TipoEmpreendimento } from 'src/app/models/tipo-empreendimento.model';
import { UsuarioImobiliaria } from 'src/app/models/usuario-imobiliaria.model';
import { EmpreendimentoService } from 'src/app/services/admin/empreendimento.service';
import { EnderecoService } from 'src/app/services/admin/endereco.service';
import {
  CidadeProps,
  EstadoProps,
  LocalizacaoService,
} from 'src/app/services/admin/localizacao.service';
import { SubtipoEmpreendimentoService } from 'src/app/services/admin/subtipo-empreendimento.service';
import { TipoEmpreendimentoService } from 'src/app/services/admin/tipo-empreendimento.service';
import { UsuarioImobiliariaService } from 'src/app/services/admin/usuario-imobiliaria.service';
import { AuthService } from 'src/app/services/auth.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { UploadService } from 'src/app/services/upload.service';
import {
  empreendimentoStatus,
  empreendimentoTiposOferta,
} from 'src/app/services/global.service';

@Component({
  selector: 'app-im-empreendimentos-criar',
  templateUrl: './im-empreendimentos-criar.component.html',
  styleUrls: ['./im-empreendimentos-criar.component.scss'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { showError: true },
    },
  ],
})
export class ImEmpreendimentosCriarComponent implements OnInit, AfterViewInit {
  @ViewChild('stepper') stepper: MatStepper;

  immobileFormGroup: UntypedFormGroup;
  addressFormGroup: UntypedFormGroup;
  featuresFormGroup: UntypedFormGroup;
  typeFormGroup: UntypedFormGroup;
  currencyFormGroup: UntypedFormGroup;

  estados: EstadoProps[] = [];
  searchEstados: string = '';

  usuariosImobiliaria: UsuarioImobiliaria[] = [];
  searchUsuariosImobiliaria: string = '';

  cidades: CidadeProps[] = [];
  searchCidades: string = '';

  subtiposEmpreendimento: SubtipoEmpreendimento[] = [];
  searchSubtiposEmpreendimento: string = '';

  tiposEmpreendimento: TipoEmpreendimento[] = [];
  searchTiposEmpreendimento: string = '';

  files: File[] = [];
  progress: number = 0;

  showMap: boolean = false;

  empreendimentoStatus = empreendimentoStatus;
  empreendimentoTiposOferta = empreendimentoTiposOferta;

  constructor(
    public _formBuilder: UntypedFormBuilder,
    public localizacaoService: LocalizacaoService,
    public helper: HelperService,
    public tipoEmpreendimentoService: TipoEmpreendimentoService,
    public subtiposEmpreendimentoService: SubtipoEmpreendimentoService,
    public empreendimentoService: EmpreendimentoService,
    public usuarioImobiliariaService: UsuarioImobiliariaService,
    public uploadService: UploadService,
    public loadingService: LoadingService,
    public enderecoService: EnderecoService,
    public router: Router,
    public auth: AuthService,
    public dialog: MatDialog
  ) {
    if (this.auth.user.tipo === 'A') {
      this.buscarUsuariosImobiliaria();
    }
  }

  buscarUsuariosImobiliaria() {
    this.usuarioImobiliariaService
      .get(-99, -99, '1', 'ASC', this.auth.user.imobiliaria.id, 'C', 'A')
      .subscribe((res) => (this.usuariosImobiliaria = res.usuarios));
  }

  ngOnInit() {
    this.buscarEstados();
    this.buscarTiposEmpreendimento();

    this.immobileFormGroup = this._formBuilder.group({
      nome: ['', Validators.required],
      descricao: ['', Validators.required],
      status: ['A', Validators.required],
      entrarEmContato: [
        this.auth.user.imobiliaria.contatoDefault,
        Validators.required,
      ],
      usuarioImobiliaria: [
        this.auth.user.tipo === 'A' ? null : this.auth.user,
        Validators.required,
      ],
      diferenciais: [''],
    });

    this.addressFormGroup = this._formBuilder.group({
      cep: ['', Validators.required],
      uf: ['', Validators.required],
      cidade: ['', Validators.required],
      bairro: ['', Validators.required],
      endereco: ['', Validators.required],
      numeroEndereco: [''],
      complemento: [''],
      latitude: [0, Validators.required],
      longitude: [0, Validators.required],
    });

    this.featuresFormGroup = this._formBuilder.group({
      numeroQuartos: [0, Validators.required],
      numeroSuites: [0, Validators.required],
      numeroBanheiros: [0, Validators.required],
      numeroBoxGaragem: [0, Validators.required],
      tamanhoTerrenoM2: [0, Validators.required],
      tamanhoAreaConstruidaM2: [0, Validators.required],
    });

    this.typeFormGroup = this._formBuilder.group({
      tipoEmpreendimento: [null, Validators.required],
      subtipoEmpreendimento: [null, Validators.required],
      tipoOferta: ['', Validators.required],
      estadoUso: ['', Validators.required],
    });

    this.currencyFormGroup = this._formBuilder.group({
      valorCondominio: [''],
      valor: ['', Validators.required],
    });
  }

  ngAfterViewInit() {}

  buscarEstados() {
    this.localizacaoService.getEstados().subscribe((res) => {
      this.estados = res.map((e) => {
        e.descricao = `${e.sigla} - ${e.nome}`;
        return e;
      });
    });
  }

  buscarCidades(siglaUf) {
    this.localizacaoService
      .getCidades(siglaUf)
      .subscribe((res) => (this.cidades = res));
  }

  buscarTiposEmpreendimento() {
    this.tipoEmpreendimentoService
      .get(-99, -99, '1', 'ASC', 'A')
      .subscribe((res) => (this.tiposEmpreendimento = res.tiposEmpreendimento));
  }

  buscarSubtiposEmpreendimento(idTipoEmpreendimento) {
    this.subtiposEmpreendimentoService
      .get(idTipoEmpreendimento)
      .subscribe((res) => (this.subtiposEmpreendimento = res));
  }

  onChangeEstado(e: MatSelectChange) {
    if (!e.value) {
      this.cidades = [];
      return;
    }
    this.buscarCidades(e.value);
  }

  onChangeTipoEmpreendimento(e: MatSelectChange) {
    if (!e.value) {
      this.subtiposEmpreendimento = [];
      return;
    }
    this.buscarSubtiposEmpreendimento(e.value.id);
  }

  @ViewChild('numeroEnderecoInput') numeroEnderecoInputRef: ElementRef;
  onBlurCep(e) {
    const cep = e.target.value.replace('-', '');
    if (cep.length < 8) return;
    this.helper.viaCep(cep).subscribe((res) => {
      if (res.uf) {
        this.buscarCidades(res.uf);
      }
      this.addressFormGroup.patchValue({
        uf: res.uf,
        cidade: res.localidade,
        bairro: res.bairro,
        endereco: res.logradouro,
      });
      this.numeroEnderecoInputRef.nativeElement.focus();
    });
    this.googleGeocode(cep);
  }

  onSelect(event) {
    this.files.push(...event.addedFiles);

    const formData = new FormData();

    for (var i = 0; i < this.files.length; i++) {
      formData.append('file[]', this.files[i]);
    }
  }

  onRemove(event) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  googleGeocode(cep: string) {
    const API_KEY = 'AIzaSyBNxHqA_kCOuCMkPpomr1bWPN5uTlgnIgU';
    const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${cep}&key=${API_KEY}`;
    fetch(url, {
      method: 'GET',
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.status === 'OK') {
          const result = res.results[0];
          if (result) {
            this.showMap = true;
            this.addressFormGroup.patchValue({
              latitude: result.geometry.location.lat,
              longitude: result.geometry.location.lng,
            });
          }
        }
      });
  }

  getLatLng(e) {
    this.addressFormGroup.patchValue({
      latitude: e.lat,
      longitude: e.lng,
    });
  }

  submit() {
    if (this.immobileFormGroup.status === 'INVALID') {
      this.helper.openSnackBar(
        'Complete os detalhes do imóvel, por favor!',
        undefined,
        true
      );
      return;
    }

    if (this.addressFormGroup.status === 'INVALID') {
      this.helper.openSnackBar(
        'Complete os dados de endereço, por favor!',
        undefined,
        true
      );
      return;
    }

    if (this.featuresFormGroup.status === 'INVALID') {
      this.helper.openSnackBar(
        'Preencha todas as características, por favor!',
        undefined,
        true
      );
      return;
    }

    if (this.typeFormGroup.status === 'INVALID') {
      this.helper.openSnackBar(
        'Complete a etapa TIPO, por favor!',
        undefined,
        true
      );
      return;
    }

    if (this.currencyFormGroup.status === 'INVALID') {
      this.helper.openSnackBar(
        'Informe o valor do seu imóvel, por favor!',
        undefined,
        true
      );
      return;
    }

    if (!this.files.length) {
      this.helper.openSnackBar(
        'Insira pelo menos uma imagem ao seu imóvel, por favor!',
        undefined,
        true
      );
      return;
    }

    const data = {
      ...new Empreendimento(),
      // immobileFormGroup
      nome: this.immobileFormGroup.get('nome').value,
      descricao: this.immobileFormGroup.get('descricao').value,
      imobiliaria: this.auth.user.imobiliaria,
      usuarioImobiliaria:
        this.auth.user.tipo === 'A'
          ? this.immobileFormGroup.get('usuarioImobiliaria').value
          : this.auth.user,
      diferenciais: this.immobileFormGroup.get('diferenciais').value,
      entrarEmContato: this.immobileFormGroup.get('entrarEmContato').value,
      // addressFormGroup
      cep: this.addressFormGroup.get('cep').value,
      uf: this.addressFormGroup.get('uf').value,
      cidade: this.addressFormGroup.get('cidade').value,
      bairro: this.addressFormGroup.get('bairro').value,
      endereco: this.addressFormGroup.get('endereco').value,
      numeroEndereco: this.addressFormGroup.get('numeroEndereco').value,
      complemento: this.addressFormGroup.get('complemento').value,
      latitude: this.addressFormGroup.get('latitude').value,
      longitude: this.addressFormGroup.get('longitude').value,
      // featuresFormGroup
      numeroQuartos: this.featuresFormGroup.get('numeroQuartos').value,
      numeroSuites: this.featuresFormGroup.get('numeroSuites').value,
      numeroBanheiros: this.featuresFormGroup.get('numeroBanheiros').value,
      numeroBoxGaragem: this.featuresFormGroup.get('numeroBoxGaragem').value,
      tamanhoTerrenoM2: this.featuresFormGroup.get('tamanhoTerrenoM2').value,
      tamanhoAreaConstruidaM2: this.featuresFormGroup.get(
        'tamanhoAreaConstruidaM2'
      ).value,
      // typeFormGroup
      tipoEmpreendimento: this.typeFormGroup.get('tipoEmpreendimento').value,
      subtipoEmpreendimento: this.typeFormGroup.get('subtipoEmpreendimento')
        .value,
      tipoOferta: this.typeFormGroup.get('tipoOferta').value,
      estadoUso: this.typeFormGroup.get('estadoUso').value,
      // currencyFormGroup
      valor: this.currencyFormGroup.get('valor').value,
      valorCondominio: this.currencyFormGroup.get('valorCondominio').value,
      // dropzone
    } as Empreendimento;

    this.loadingService.present('Buscando endereço...');

    this.enderecoService
      .get(
        data.cep,
        data.uf,
        data.cidade,
        data.bairro,
        data.endereco,
        data.numeroEndereco,
        data.complemento
      )
      .subscribe(
        (res) => {
          if (!res || !res?.id) {
            this.enderecoService
              .getWithOutComplement(
                data.cep,
                data.uf,
                data.cidade,
                data.bairro,
                data.endereco,
                data.numeroEndereco
              )
              .subscribe(
                (res) => {
                  if (!res.length) {
                    data.idEndereco = null;
                    this.submitFunction(data);
                  } else {
                    this.loadingService.dismiss();
                    this.openAddressDialog(data, res);
                  }
                },
                (e) => this.loadingService.dismiss()
              );
          } else {
            data.idEndereco = res?.id;
            this.submitFunction(data);
          }
        },
        (e) => this.loadingService.dismiss()
      );
  }

  openAddressDialog(data: Empreendimento, res: Endereco[]) {
    const dialogRef = this.dialog.open(AddressDialogComponent, {
      width: '400px',
      data: res,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        data.idEndereco = result === 'Outro' ? null : result;
        this.submitFunction(data);
      }
    });
  }

  submitFunction(data: Empreendimento) {
    this.loadingService.present('Inserindo empreendimento...');

    this.empreendimentoService.post(data).subscribe(
      (res) => {
        this.uploadService
          .uploadMultipleFiles(
            this.files,
            `/imobiliaria/empreendimentoImagem/inserirMultiplo?idEmpreendimento=${res}`
          )
          .subscribe(
            (event: HttpProgressEvent | any) => {
              if (event.type === 4) {
                this.progress = 0;
                this.router.navigate([
                  '/imobiliaria/empreendimentos/editar',
                  res,
                ]);
                this.loadingService.dismiss();
              } else {
                this.progress = Math.round((event.loaded / event.total) * 100);
                if (isNaN(this.progress)) {
                  this.progress = 100;
                }
                this.loadingService.title = `${this.progress}%`;
              }
            },
            (e) => this.loadingService.dismiss()
          );
      },
      (e) => this.loadingService.dismiss()
    );
  }

  onNumeroEnderecoBlur(e) {
    if (
      !this.addressFormGroup.get('endereco').value ||
      !this.addressFormGroup.get('bairro').value ||
      !this.addressFormGroup.get('cidade').value ||
      !this.addressFormGroup.get('uf').value
    ) {
      return;
    }

    const field = `${this.addressFormGroup.get('numeroEndereco').value}+${
      this.addressFormGroup.get('endereco').value
    }+${this.addressFormGroup.get('bairro').value}+${
      this.addressFormGroup.get('cidade').value
    }+${this.addressFormGroup.get('uf').value}`;
    this.googleGeocode(field);
  }
}
