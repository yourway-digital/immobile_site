import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, ElementRef, EventEmitter, Inject, Input, OnChanges, OnInit, Output, SimpleChange, ViewChild } from '@angular/core';
import { Loader, LoaderOptions } from '@googlemaps/js-api-loader';
import styles from '../../../../styles/map-styles';

declare var google: any;

interface LatLng {
  lat: number;
  lng: number;
}

@Component({
  selector: 'app-im-map',
  templateUrl: './im-map.component.html',
  styleUrls: ['./im-map.component.scss']
})
export class ImMapComponent implements OnInit, AfterViewInit, OnChanges {

  @Input('lat') lat: number;
  @Input('lng') lng: number;
  @Output() event = new EventEmitter<LatLng>();

  marker;

  constructor(
    @Inject(DOCUMENT) private document: Document,
  ) { }

  ngOnInit(): void {
    this.initMap(this.lat, this.lng)
  }

  ngOnChanges(changes) {
    if (!this.marker) {
      return;
    }
    if ((changes['lat'].currentValue !== changes['lat'].previousValue) || (changes['lng'].currentValue !== changes['lng'].previousValue)) {
      const latlng = new google.maps.LatLng(this.lat, this.lng);
      this.marker.setPosition(latlng);
      this.event.emit({
        lat: this.lat,
        lng: this.lng
      })
    }
  }

  ngAfterViewInit() {
  }


  @ViewChild('map') mapRef: ElementRef;
  initMap(lat, lng) {

    const latLng = { lat: lat, lng: lng }

    const loader = new Loader({
      apiKey: "AIzaSyBNxHqA_kCOuCMkPpomr1bWPN5uTlgnIgU",
      version: "weekly",
      language: 'pt-Br',
      libraries: ['visualization', 'places']
    } as LoaderOptions);

    loader.load().then(() => {
      const map = new google.maps.Map(this.mapRef.nativeElement, {
        center: latLng,
        zoom: 17,
        styles: styles,
      });

      this.marker = new google.maps.Marker({
        title: 'Telmaster',
        position: latLng,
        map: map,
        animation: google.maps.Animation.DROP,
        draggable: true,
      })

      this.marker.addListener('dragend', (e) => {
        this.lat = e.latLng.lat();
        this.lng = e.latLng.lng();
        this.event.emit({
          lat: e.latLng.lat(),
          lng: e.latLng.lng()
        })
      })

    });
  }


}
