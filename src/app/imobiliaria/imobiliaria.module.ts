import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImobiliariaRoutingModule } from './imobiliaria-routing.module';
import { MainComponent } from './main/main.component';
import { HomeComponent } from './pages/home/home.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './components/header/header.component';
import { PipesModule } from '../pipes/pipes.module';
import { ImEmpreendimentosCriarComponent } from './pages/im-empreendimentos-criar/im-empreendimentos-criar.component';
import { ImEmpreendimentosEditarComponent } from './pages/im-empreendimentos-editar/im-empreendimentos-editar.component';
import { ImMapComponent } from './components/im-map/im-map.component';
import { NgxMaskModule } from 'ngx-mask';
import { NgxCurrencyModule } from 'ngx-currency';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { ImUsuariosComponent } from './pages/im-usuarios/im-usuarios.component';
import { ImUsuariosCriarComponent } from './pages/im-usuarios-criar/im-usuarios-criar.component';
import { ImUsuariosEditarComponent } from './pages/im-usuarios-editar/im-usuarios-editar.component';
import { ImEmpreendimentosComponent } from './pages/im-empreendimentos/im-empreendimentos.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { GalleryModule } from '@ks89/angular-modal-gallery';
import { ImEstatisticasComponent } from './pages/im-estatisticas/im-estatisticas.component';
import { SharedModule } from '../shared/shared.module';
import { NgxDropzoneModule } from 'ngx-dropzone';

@NgModule({
  declarations: [
    MainComponent,
    HomeComponent,
    HeaderComponent,
    ImEmpreendimentosCriarComponent,
    ImEmpreendimentosEditarComponent,
    ImMapComponent,
    ImUsuariosComponent,
    ImUsuariosCriarComponent,
    ImUsuariosEditarComponent,
    ImEmpreendimentosComponent,
    PerfilComponent,
    ImEstatisticasComponent,
  ],
  imports: [
    CommonModule,
    ImobiliariaRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
    NgxMaskModule.forChild(),
    NgxCurrencyModule,
    NgxMatSelectSearchModule,
    GalleryModule,
    SharedModule,
    NgxDropzoneModule,
  ],
})
export class ImobiliariaModule {}
