import { Autenticacao } from './autenticacao.model';
import { Imobiliaria } from './imobiliaria.model';

export class UsuarioImobiliaria {
  id = 0;
  nome = '';
  email = '';
  senha = '';
  re_senha = '';
  situacao = 'A';
  tipo = 'C'; // A - Admin, C - Corretor
  limiteOfertas = 0;
  whatsapp = '';
  _ofertasValidas = 0;
  imobiliaria?: Imobiliaria;
  autenticacao?: Autenticacao;
  // aux
  loading? = false;
}
