import { Plano } from './plano.model';

export class Imobiliaria {
  id: number = 0;
  nome = '';
  descricao = '';
  razaoSocial = '';
  cnpj = '';
  telefone = '';
  whatsapp = '';
  email = '';
  dataHoraCriacao: Date = new Date();
  imagem = '';
  status: string = 'A';
  situacao: string = 'A';
  plano: Plano;
  // endereco
  cep = '';
  uf = '';
  cidade = '';
  bairro = '';
  endereco = '';
  enderecoNumero = '';
  complemento = '';
  contatoDefault = 'C';
  observacao = '';

  constructor() {
    Object.assign(this);
  }
}
