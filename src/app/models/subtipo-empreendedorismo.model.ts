export class SubtipoEmpreendimento {

    id: number = 0;
    nome: string = '';
    situacao: string = 'A';
    idTipoEmpreendimento: number = 0;
    [x: string]: any;
    
    constructor() {
        Object.assign(this);
    }
}