export class Endereco {
  id: number = 0;
  bairro: string = '';
  cep: string = '';
  cidade: string = '';
  complemento: string = '';
  endereco: string = '';
  numeroEndereco: string = '';
  pais: string = '';
  uf: string = '';
  [x: string]: any;
  constructor() {
    Object.assign(this);
  }
}
