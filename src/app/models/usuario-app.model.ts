import { Autenticacao } from "./autenticacao.model";

export class UsuarioApp {

    id: number = 0;
    nome: string = '';
    email: string = '';
    senha: string = '';
    situacao: string = 'A';
    autenticacao: Autenticacao = new Autenticacao();
    [x: string]: any;
    
    constructor() {
        Object.assign(this);
    }
}