import { EmpreendimentoImagem } from './empreendimento-imagem.model';
import { Imobiliaria } from './imobiliaria.model';
import { SubtipoEmpreendimento } from './subtipo-empreendedorismo.model';
import { TipoEmpreendimento } from './tipo-empreendimento.model';
import { UsuarioImobiliaria } from './usuario-imobiliaria.model';

export class Empreendimento {
  id = 0;
  nome = '';
  descricao = '';
  diferenciais? = ''; // opcional
  cep = '';
  uf = '';
  _favorito = '';
  idEndereco = 0;
  cidade = '';
  pais = 'BR';
  bairro = '';
  endereco = '';
  numeroEndereco = ''; // revisar
  enderecoNumero = ''; // revisar
  entrarEmContato = '';
  complemento = ''; // opcional
  latitude = 0;
  longitude = 0;
  valor = 0;
  valorCondominio? = 0; // opcional
  numeroQuartos = 0;
  numeroSuites = 0;
  numeroBanheiros = 0;
  numeroBoxGaragem = 0;
  tamanhoTerrenoM2 = 0;
  tamanhoAreaConstruidaM2 = 0;
  dataHoraCriacao = new Date();
  situacao = 'A';
  status = 'A'; // A - Aberto, V - Vendido, L - Locado
  tipoOferta = ''; // V - Venda, L - Locação
  estadoUso = ''; // N - Novo, U - Usado, P - Na platan
  _aparicoes = 0;
  _clicks = 0;
  _contatos = 0;
  _imagemPrincipal = ''; // campo aux
  tipoEmpreendimento?: TipoEmpreendimento;
  subtipoEmpreendimento?: SubtipoEmpreendimento;
  imagens: EmpreendimentoImagem[] = [];
  usuarioImobiliaria?: UsuarioImobiliaria;
  imobiliaria?: Imobiliaria;
  slug = '';
}
