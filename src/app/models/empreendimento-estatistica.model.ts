import { Empreendimento } from "./empreendimento.model";
import { UsuarioApp } from "./usuario-app.model";

export class EmpreendimentoEstatistica {

    id: number = 0;
    empreendimento: Empreendimento = new Empreendimento();
    dataHora: Date = new Date();
    tipo: string = '';
    usuarioApp: UsuarioApp = new UsuarioApp();
    idDispositivo: string;
    [x: string]: any;
    
    constructor() {
        Object.assign(this);
    }
}