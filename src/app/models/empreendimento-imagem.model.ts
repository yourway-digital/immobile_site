export class EmpreendimentoImagem {

    id: number = 0;
    nome: string = '';
    descricao: string = '';
    imagem: string = '';
    principal: string = 'N';
    ordem: number = -99;
    idEmpreendimento: number = 0;
    [x: string]: any;
    constructor() {
        Object.assign(this);
    }
}