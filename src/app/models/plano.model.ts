export class Plano {

    id: number = 0;
    nome: string = '';
    descricao: string = '';
    limiteOfertas: 4;
    valor: 5;
    situacao: string = 'A';
    [x: string]: any;

    constructor() {
        Object.assign(this);
    }
}