import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root',
})
export class EnderecoService {
  constructor(public api: ApiService, public auth: AuthService) {}

  get(
    cep: string,
    uf: string,
    cidade: string,
    bairro: string,
    endereco: string,
    numeroEndereco: string,
    complemento?: string
  ): Observable<any> {
    return this.api.get(`/${this.auth.type}/endereco/buscar`, {
      cep: cep,
      uf: uf,
      bairro: bairro,
      endereco: endereco,
      numeroEndereco: numeroEndereco,
      complemento: complemento ? complemento : '%',
      cidade: cidade,
    });
  }

  getWithOutComplement(
    cep: string,
    uf: string,
    cidade: string,
    bairro: string,
    endereco: string,
    numeroEndereco: string
  ): Observable<any> {
    return this.api.get(`/${this.auth.type}/endereco/buscar`, {
      cep: cep,
      uf: uf,
      bairro: bairro,
      endereco: endereco,
      numeroEndereco: numeroEndereco,
      cidade: cidade,
    });
  }
}
