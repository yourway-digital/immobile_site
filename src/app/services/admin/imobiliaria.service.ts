import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Imobiliaria } from 'src/app/models/imobiliaria.model';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';
import { HelperService } from '../helper.service';

@Injectable({
  providedIn: 'root',
})
export class ImobiliariaService {
  constructor(
    public api: ApiService,
    public auth: AuthService,
    private helper: HelperService
  ) {}

  get(
    numeroPagina: number,
    registroPorPagina: number,
    ordenacao: string,
    sentidoOrdenacao: string,
    situacao: string = '%',
    filtro: string = '%'
  ): Observable<ImobiliariasApi> {
    const params = {
      filtro: filtro,
      numeroPagina: numeroPagina,
      registroPorPagina: registroPorPagina,
      ordenacao: ordenacao,
      sentidoOrdenacao: sentidoOrdenacao,
      situacao: situacao ? situacao : '%',
    };
    return this.api.get(`/${this.auth.type}/imobiliaria/buscar`, params) as any;
  }

  getById(id): Observable<any> {
    return this.api.get(`/${this.auth.type}/imobiliaria/buscar`, { id: id });
  }

  post(imobiliaria: Imobiliaria): Observable<any> {
    return this.api.post(`/${this.auth.type}/imobiliaria/inserir`, imobiliaria);
  }

  patch(imobiliaria: Imobiliaria): Observable<any> {
    return this.api.post(`/${this.auth.type}/imobiliaria/alterar`, imobiliaria);
  }

  delete(imobiliaria: Imobiliaria): Observable<any> {
    return this.api.post(`/${this.auth.type}/imobiliaria/deletar`, imobiliaria);
  }

  deleteSelected(imobiliarias: Imobiliaria[]): Observable<any> {
    return this.api.post(
      `/${this.auth.type}/imobiliaria/deletarLista`,
      imobiliarias
    );
  }

  compareFn(v1: Imobiliaria, v2: Imobiliaria): boolean {
    return v1 && v2 ? v1.id === v2.id : v1 === v2;
  }

  generateExcel(
    idImobiliaria?: number,
    dataInicial?: Date,
    dataFinal?: Date
  ): Observable<any> {
    const params = {
      idImobiliaria,
      dataInicial: dataInicial
        ? this.helper.moment(dataInicial).format('YYYY-MM-DD')
        : '',
      dataFinal: dataFinal
        ? this.helper.moment(dataFinal).format('YYYY-MM-DD')
        : '',
    };

    return this.api.get(`/${this.auth.type}/estatistica/gerarExcel`, params);
  }
}

export interface ImobiliariasApi {
  imobiliarias: Imobiliaria[];
  numeroPaginas: number;
}
