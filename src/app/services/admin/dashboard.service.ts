import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';

export type DashboardType = 'APARICOES' | 'CLICKS' | 'CONTATOS' | 'ACESSOAPP';
export type LocationType = 'P' | 'E' | 'C' | 'B' | 'R';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(public api: ApiService) {}

  getByDay(
    tipoEstatistica: DashboardType,
    dataInicial: string,
    dataFinal: string
  ) {
    return this.api.get(`/gerenciador/dashboard/buscarEstatisticasDia`, {
      tipoEstatistica: tipoEstatistica,
      dataInicial,
      dataFinal,
    });
  }

  getByMonth(
    tipoEstatistica: DashboardType,
    dataInicial: string,
    dataFinal: string
  ) {
    return this.api.get(`/gerenciador/dashboard/buscarEstatisticasMes`, {
      tipoEstatistica: tipoEstatistica,
      dataInicial,
      dataFinal,
    });
  }

  getByYear(
    tipoEstatistica: DashboardType,
    dataInicial: string,
    dataFinal: string
  ) {
    return this.api.get(`/gerenciador/dashboard/buscarEstatisticasAno`, {
      tipoEstatistica: tipoEstatistica,
      dataInicial,
      dataFinal,
    });
  }

  getByLocation(tipo: LocationType) {
    return this.api.get(`/gerenciador/dashboard/buscarPorLocalidade`, {
      tipo: tipo,
    });
  }
}
