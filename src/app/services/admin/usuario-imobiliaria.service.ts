import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UsuarioImobiliaria } from 'src/app/models/usuario-imobiliaria.model';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioImobiliariaService {

  constructor(
    public api: ApiService,
    public auth: AuthService,
  ) { }

  get(numeroPagina: number, registroPorPagina: number, ordenacao: string, sentidoOrdenacao: string, idImobiliaria: any, tipo: string = '%', situacao: string = 'A', filtro: string = '%'): Observable<UsuariosApi> {
    const params = {
      filtro: filtro,
      numeroPagina: numeroPagina,
      registroPorPagina: registroPorPagina,
      ordenacao: ordenacao,
      sentidoOrdenacao: sentidoOrdenacao,
      situacao: situacao,
      idImobiliaria: this.auth.type === 'imobiliaria' ? this.auth.user.imobiliaria.id : idImobiliaria,
      tipo: tipo,
    }
    return this.api.get(`/${this.auth.type}/usuarioImobiliaria/buscar`, params) as any;
  }

  getById(id): Observable<any> {
    return this.api.get(`/${this.auth.type}/usuarioImobiliaria/buscar`, { id: id });
  }

  getPassword(id): Observable<any> {
    return this.api.get(`/${this.auth.type}/usuarioImobiliaria/buscarSenha`, { id: id })
  }

  emailExists(id, email, operacao = 'insert'): Observable<any> {
    return this.api.get(`/${this.auth.type}/usuarioImobiliaria/existeEmail`, { id: id, email: email, operacao: operacao });
  }

  post(user: UsuarioImobiliaria): Observable<any> {
    return this.api.post(`/${this.auth.type}/usuarioImobiliaria/inserir`, user);
  }

  patch(user: UsuarioImobiliaria): Observable<any> {
    return this.api.post(`/${this.auth.type}/usuarioImobiliaria/alterar`, user);
  }

  delete(user: UsuarioImobiliaria): Observable<any> {
    return this.api.post(`/${this.auth.type}/usuarioImobiliaria/deletar`, user);
  }

  deleteSelected(users: UsuarioImobiliaria[]): Observable<any> {
    return this.api.post(`/${this.auth.type}/usuarioImobiliaria/deletarLista`, users);
  }

  changePassword(usuario: string, senha: string) {
    return this.api.get(`/${this.auth.type}/usuarioImobiliaria/alterarSenha`, {
      usuario: usuario,
      senha: senha
    })
  }

  hasCredit(idUsuarioImobiliaria: number, idEmpreendimento: number = null): Observable<any> {
    return this.api.get(`/${this.auth.type}/usuarioImobiliaria/temCreditos`, {
      idUsuarioImobiliaria: idUsuarioImobiliaria,
      idEmpreendimento: idEmpreendimento
    })
  }

  distributeCredits(idImobiliaria: number, usuariosImobiliaria: UsuarioImobiliaria[]) {
    return this.api.post(`/${this.auth.type}/usuarioImobiliaria/distribuirCreditos`, usuariosImobiliaria, {
      idImobiliaria: idImobiliaria,
    })
  }

  compareFn(v1: UsuarioImobiliaria, v2: UsuarioImobiliaria): boolean {
    return v1 && v2 ? v1.id === v2.id : v1 === v2;
  }
}

export interface UsuariosApi {
  usuarios: UsuarioImobiliaria[];
  numeroPaginas: number;
}
