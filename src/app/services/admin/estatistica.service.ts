import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmpreendimentoEstatistica } from 'src/app/models/empreendimento-estatistica.model';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root',
})
export class EstatisticaService {
  constructor(public api: ApiService, public auth: AuthService) {}

  getData(
    numeroPagina: number,
    registroPorPagina: number,
    tipo: string,
    idEmpreendimento: number,
    idUsuarioApp: number,
    dataInicial: Date,
    dataFinal: Date
  ): Observable<EmpreendimentoEstatistica[]> {
    return this.api.get(`/${this.auth.type}/estatistica/buscarDados`, {
      numeroPagina: numeroPagina,
      registroPorPagina: registroPorPagina,
      tipo: tipo ? tipo : '%',
      idEmpreendimento: idEmpreendimento,
      idUsuarioApp: idUsuarioApp ? idUsuarioApp : '%',
      dataInicial: dataInicial ? dataInicial : '%',
      dataFinal: dataFinal ? dataFinal : '%',
    }) as any;
  }

  getAccountants(
    idImobiliaria: number,
    idUsuarioCorretor: number,
    dataInicial: Date,
    dataFinal: Date
  ) {
    return this.api.get<HomeStats>(
      `/${this.auth.type}/estatistica/buscarContadores`,
      {
        idImobiliaria: idImobiliaria,
        idUsuarioCorretor: idUsuarioCorretor ? idUsuarioCorretor : '%',
        dataInicial: dataInicial ? dataInicial : '%',
        dataFinal: dataFinal ? dataFinal : '%',
      }
    );
  }

  getAccountantsAdmin(dataInicial: Date, dataFinal: Date) {
    return this.api.get(`/${this.auth.type}/estatistica/buscarContadores`, {
      dataInicial: dataInicial ? dataInicial : '%',
      dataFinal: dataFinal ? dataFinal : '%',
    });
  }

  getCountInactiveUsers() {
    return this.api.get(`/gerenciador/estatistica/countUsuariosInativos`);
  }

  getCountActiveUsers() {
    return this.api.get(`/gerenciador/estatistica/countUsuariosAtivos`);
  }
}

interface HomeStats {
  aparicoes: number;
  clicks: number;
  contatos: number;
}
