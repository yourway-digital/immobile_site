import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UsuarioApp } from 'src/app/models/usuario-app.model';
import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioAppService {

  constructor(
    public api: ApiService
  ) { }

  get(numeroPagina: number, registroPorPagina: number, ordenacao: string, sentidoOrdenacao: string, situacao: string = 'A', filtro: string = '%'): Observable<UsuariosApi> {
    const params = {
      filtro: filtro,
      numeroPagina: numeroPagina,
      registroPorPagina: registroPorPagina,
      ordenacao: ordenacao,
      sentidoOrdenacao: sentidoOrdenacao,
      situacao: situacao,
    }
    return this.api.get(`/gerenciador/usuarioApp/buscar`, params) as any;
  }

  getById(id): Observable<any> {
    return this.api.get(`/gerenciador/usuarioApp/buscar`, { id: id });
  }
}

export interface UsuariosApi {
  usuarios: UsuarioApp[];
  numeroPaginas: number;
}
