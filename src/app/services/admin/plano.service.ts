import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Plano } from 'src/app/models/plano.model';
import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root'
})
export class PlanoService {

  constructor(
    public api: ApiService
  ) { }

  get(numeroPagina: number, registroPorPagina: number, ordenacao: string, sentidoOrdenacao: string, situacao: string = 'A', filtro: string = '%'): Observable<PlanosApi> {
    const params = {
      filtro: filtro,
      numeroPagina: numeroPagina,
      registroPorPagina: registroPorPagina,
      ordenacao: ordenacao,
      sentidoOrdenacao: sentidoOrdenacao,
      situacao: situacao,
    }
    return this.api.get(`/gerenciador/plano/buscar`, params) as any;
  }

  getById(id): Observable<any> {
    return this.api.get(`/gerenciador/plano/buscar`, { id: id });
  }

  post(plano: Plano): Observable<any> {
    return this.api.post('/gerenciador/plano/inserir', plano);
  }

  patch(plano: Plano): Observable<any> {
    return this.api.post('/gerenciador/plano/alterar', plano);
  }

  delete(plano: Plano): Observable<any> {
    return this.api.post('/gerenciador/plano/deletar', plano);
  }

  deleteSelected(planos: Plano[]): Observable<any> {
    return this.api.post('/gerenciador/plano/deletarLista', planos);
  }

  compareFn(v1: Plano, v2: Plano): boolean {
    return v1 && v2 ? v1.id === v2.id : v1 === v2;
  }
}

export interface PlanosApi {
  planos: Plano[];
  numeroPaginas: number;
}
