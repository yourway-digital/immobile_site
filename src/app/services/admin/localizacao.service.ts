import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root',
})
export class LocalizacaoService {
  constructor(public api: ApiService) {}

  getEstados() {
    return this.api.get<EstadoProps[]>('/localizacao/uf/buscar');
  }

  getCidades(siglaUf: string, filtro: string = '') {
    return this.api.get<CidadeProps[]>('/localizacao/cidade/buscar', {
      siglaUf: siglaUf,
      filtro: filtro,
    });
  }
}

export interface EstadoProps {
  id: number;
  nome: string;
  sigla: string;
  descricao: string;
}

export interface CidadeProps {
  id: number;
  nome: string;
}
