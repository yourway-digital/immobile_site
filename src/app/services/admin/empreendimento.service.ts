import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Empreendimento } from 'src/app/models/empreendimento.model';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root',
})
export class EmpreendimentoService {
  constructor(public api: ApiService, public auth: AuthService) { }

  get(
    numeroPagina: number,
    registroPorPagina: number,
    ordenacao: string,
    sentidoOrdenacao: string,
    idImobiliaria: any,
    status: string,
    tipoOferta: string,
    idTipoEmpreendimento: number,
    idUsuarioImobiliaria: any,
    situacao: string = '%',
    filtro: string = '%',
    mostraEstatistica = false
  ): Observable<EmpreendimentosApi> {
    const params = {
      filtro: filtro,
      numeroPagina: numeroPagina,
      registroPorPagina: registroPorPagina,
      ordenacao: ordenacao,
      sentidoOrdenacao: sentidoOrdenacao,
      situacao: situacao ? situacao : '%',
      idImobiliaria: idImobiliaria ? idImobiliaria : '%',
      status: status ? status : '%',
      tipoOferta: tipoOferta ? tipoOferta : '%',
      idTipoEmpreendimento: idTipoEmpreendimento ? idTipoEmpreendimento : '%',
      idUsuarioImobiliaria,
      mostraEstatistica
    };
    return this.api.get(
      `/${this.auth.type}/empreendimento/buscar`,
      params
    ) as any;
  }

  getById(id): Observable<Empreendimento> {
    return this.api.get(`/${this.auth.type}/empreendimento/buscar`, {
      id: id,
    }) as any;
  }

  post(empreendimeno: Empreendimento): Observable<any> {
    return this.api.post(
      `/${this.auth.type}/empreendimento/inserir`,
      empreendimeno
    );
  }

  patch(empreendimeno: Empreendimento): Observable<any> {
    return this.api.post(
      `/${this.auth.type}/empreendimento/alterar`,
      empreendimeno
    );
  }

  delete(empreendimeno: Empreendimento): Observable<any> {
    return this.api.post(
      `/${this.auth.type}/empreendimento/deletar`,
      empreendimeno
    );
  }

  deleteSelected(empreendimenos: Empreendimento[]): Observable<any> {
    return this.api.post(
      `/${this.auth.type}/empreendimento/deletarLista`,
      empreendimenos
    );
  }

  sendExpoNotification(id: number): Observable<any> {
    return this.api.get(
      `/${this.auth.type}/empreendimento/enviarNotificacaoNovoImovel`,
      { id }
    );
  }


  generateFacebookFile(): Observable<any> {
    return this.api.get(
      `/${this.auth.type}/empreendimento/GerarArquivoFacebook`
    );
  }

}

export interface EmpreendimentosApi {
  empreendimentos: Empreendimento[];
  numeroPaginas: number;
}
