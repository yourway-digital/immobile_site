import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Imobiliaria } from 'src/app/models/imobiliaria.model';
import { TipoEmpreendimento } from 'src/app/models/tipo-empreendimento.model';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class TipoEmpreendimentoService {

  constructor(
    public api: ApiService,
    public auth: AuthService,
  ) { }

  get(numeroPagina: number, registroPorPagina: number, ordenacao: string, sentidoOrdenacao: string, situacao: string = '%', filtro: string = '%'): Observable<TipoEmpreendimentosApi> {
    const params = {
      filtro: filtro,
      numeroPagina: numeroPagina,
      registroPorPagina: registroPorPagina,
      ordenacao: ordenacao,
      sentidoOrdenacao: sentidoOrdenacao,
      situacao: situacao,
    }
    return this.api.get(`/${this.auth.type}/tipoEmpreendimento/buscar`, params) as any;
  }

  getById(id): Observable<any> {
    return this.api.get(`/${this.auth.type}/tipoEmpreendimento/buscar`, { id: id });
  }

  post(tipoEmpreendimeno: TipoEmpreendimento): Observable<any> {
    return this.api.post('/gerenciador/tipoEmpreendimento/inserir', tipoEmpreendimeno);
  }

  patch(tipoEmpreendimeno: TipoEmpreendimento): Observable<any> {
    return this.api.post('/gerenciador/tipoEmpreendimento/alterar', tipoEmpreendimeno);
  }

  delete(tipoEmpreendimeno: TipoEmpreendimento): Observable<any> {
    return this.api.post('/gerenciador/tipoEmpreendimento/deletar', tipoEmpreendimeno);
  }

  deleteSelected(tiposEmpreendimento: TipoEmpreendimento[]): Observable<any> {
    return this.api.post('/gerenciador/tipoEmpreendimento/deletarLista', tiposEmpreendimento);
  }

  compareFn(v1: TipoEmpreendimento, v2: TipoEmpreendimento): boolean {
    return v1 && v2 ? v1.id === v2.id : v1 === v2;
  }
}

export interface TipoEmpreendimentosApi {
  tiposEmpreendimento: TipoEmpreendimento[];
  numeroPaginas: number;
}
