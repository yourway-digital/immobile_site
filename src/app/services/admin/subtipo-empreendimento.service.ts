import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SubtipoEmpreendimento } from 'src/app/models/subtipo-empreendedorismo.model';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class SubtipoEmpreendimentoService {

  constructor(
    public api: ApiService,
    public auth: AuthService
  ) { }

  get(idTipoEmpreendimento: number, situacao: string = '%'): Observable<SubtipoEmpreendimento[]> {
    const params = {
      idTipoEmpreendimento: idTipoEmpreendimento,
      situacao: situacao
    }
    return this.api.get(`/${this.auth.type}/subtipoEmpreendimento/buscar`, params) as any;
  }

  getById(id): Observable<any> {
    return this.api.get(`/${this.auth.type}/subtipoEmpreendimento/buscar`, { id: id });
  }

  post(subtipoEmpreendimeno: SubtipoEmpreendimento): Observable<any> {
    return this.api.post(`/${this.auth.type}/subtipoEmpreendimento/inserir`, subtipoEmpreendimeno);
  }

  patch(subtipoEmpreendimeno: SubtipoEmpreendimento): Observable<any> {
    return this.api.post(`/${this.auth.type}/subtipoEmpreendimento/alterar`, subtipoEmpreendimeno);
  }

  delete(subtipoEmpreendimeno: SubtipoEmpreendimento): Observable<any> {
    return this.api.post(`/${this.auth.type}/subtipoEmpreendimento/deletar`, subtipoEmpreendimeno);
  }

  deleteSelected(subtipoEmpreendimenos: SubtipoEmpreendimento[]): Observable<any> {
    return this.api.post(`/${this.auth.type}/subtipoEmpreendimento/deletarLista`, subtipoEmpreendimenos);
  }

  compareFn(v1: SubtipoEmpreendimento, v2: SubtipoEmpreendimento): boolean {
    return v1 && v2 ? v1.id === v2.id : v1 === v2;
  }
}
