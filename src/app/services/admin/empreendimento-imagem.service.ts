import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmpreendimentoImagem } from 'src/app/models/empreendimento-imagem.model';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class EmpreendimentoImagemService {

  constructor(
    public api: ApiService,
    public auth: AuthService
  ) { }

  get(idEmpreendimento: number): Observable<EmpreendimentoImagem[]> {
    return this.api.get(`/${this.auth.type}/empreendimentoImagem/buscar`, { idEmpreendimento: idEmpreendimento }) as any;
  }

  delete(id: number) {
    return this.api.get(`/${this.auth.type}/empreendimentoImagem/deletar`, { id: id })
  }

  setPrincipal(id: number) {
    return this.api.get(`/${this.auth.type}/empreendimentoImagem/alterarPrincipal`, { id: id })
  }

  order(imagens: EmpreendimentoImagem[]) {
    return this.api.post(`/${this.auth.type}/empreendimentoImagem/ordenar`, imagens);
  }
}
