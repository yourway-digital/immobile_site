import { Observable } from 'rxjs';
import { ApiService } from './../api.service';
import { Injectable } from '@angular/core';
import { UsuarioAdmin } from 'src/app/models/usuario-admin.model';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    public api: ApiService
  ) { }

  get(numeroPagina: number, registroPorPagina: number, ordenacao: string, sentidoOrdenacao: string, situacao: string = 'A', filtro: string = '%'): Observable<UsuariosApi> {
    const params = {
      filtro: filtro,
      numeroPagina: numeroPagina,
      registroPorPagina: registroPorPagina,
      ordenacao: ordenacao,
      sentidoOrdenacao: sentidoOrdenacao,
      situacao: situacao,
    }
    return this.api.get(`/gerenciador/usuarioAdmin/buscar`, params) as any;
  }

  getById(id): Observable<any> {
    return this.api.get(`/gerenciador/usuarioAdmin/buscar`, { id: id });
  }

  getPassword(id): Observable<any> {
    return this.api.get(`/gerenciador/usuarioAdmin/buscarSenha`, { id: id });
  }

  getByEmail(email): Observable<any> {
    return this.api.get(`/gerenciador/usuarioAdmin/buscar`, { email: email });
  }

  post(user: UsuarioAdmin): Observable<any> {
    return this.api.post('/gerenciador/usuarioAdmin/inserir', user);
  }

  patch(user: UsuarioAdmin): Observable<any> {
    return this.api.post('/gerenciador/usuarioAdmin/alterar', user);
  }

  delete(user: UsuarioAdmin): Observable<any> {
    return this.api.post('/gerenciador/usuarioAdmin/deletar', user);
  }

  deleteSelected(users: UsuarioAdmin[]): Observable<any> {
    return this.api.post('/gerenciador/usuarioAdmin/deletarLista', users);
  }

  changePassword(usuario: string, senha: string) {
    return this.api.get(`/gerenciador/usuarioAdmin/alterarSenha?usuario=${usuario}&senha=${senha}`)
  }
}

export interface UsuariosApi {
  usuarios: UsuarioAdmin[];
  numeroPaginas: number;
}
