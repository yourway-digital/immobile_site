import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Empreendimento } from 'src/app/models/empreendimento.model';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root',
})
export class EmpreendimentoService {
  constructor(public api: ApiService, public auth: AuthService) {}

  getById(props: {
    id: number;
    idUsuarioApp?: number;
    idDispositivo?: any;
  }): Observable<Empreendimento> {
    const params = {
      idUsuarioApp: 0,
      idDispositivo: 0,
      ...props,
    };

    return this.api.get(`/app/empreendimento/buscar`, params) as any;
  }

  getContacts(idEndereco: number): Observable<Empreendimento[]> {
    return this.api.get('/app/empreendimento/buscarContato', {
      idEndereco,
    }) as any;
  }
}

export interface EmpreendimentosApi {
  empreendimentos: Empreendimento[];
  numeroPaginas: number;
}
