import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { SubtipoEmpreendimento } from 'src/app/models/subtipo-empreendedorismo.model';

@Component({
  selector: 'app-subtipo-dialog',
  templateUrl: './subtipo-dialog.component.html',
  styleUrls: ['./subtipo-dialog.component.scss']
})
export class SubtipoDialogComponent implements OnInit {

  data: SubtipoEmpreendimento = new SubtipoEmpreendimento();

  constructor(
    public dialogRef: MatDialogRef<SubtipoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: SubtipoEmpreendimento,
  ) {
    Object.assign(this.data, _data);
  }

  ngOnInit(): void {
  }

  close(status: boolean) {
    if (status) {
      this.dialogRef.close(this.data);
    } else {
      this.dialogRef.close(false);
    }
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? this.data.situacao = 'A' : this.data.situacao = 'I';
  }

}
