import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UsuarioImobiliaria } from 'src/app/models/usuario-imobiliaria.model';
import { UsuarioImobiliariaService } from 'src/app/services/admin/usuario-imobiliaria.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-credits-dialog',
  templateUrl: './credits-dialog.component.html',
  styleUrls: ['./credits-dialog.component.scss']
})
export class CreditsDialogComponent implements OnInit {

  usuarios: UsuarioImobiliaria[] = [];

  constructor(
    public dialogRef: MatDialogRef<CreditsDialogComponent>,
    public usuarioImobiliariaService: UsuarioImobiliariaService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public helper: HelperService,
    public loadingService: LoadingService,

  ) { }

  ngOnInit() {
    this.buscarUsuarios();
  }

  onClose(f: NgForm, status?: boolean): void {
    if (status) {

      if (f.invalid) {
        this.helper.formMarkAllTouched(f);
        this.helper.openSnackBar('Formulário inválido', 'Preencha os campos requiridos, por favor!', true);
        return;
      }

      this.loadingService.present('Distribuindo créditos...');

      this.usuarioImobiliariaService.distributeCredits(this.data.id, this.usuarios)
        .subscribe(res => {
          this.helper.openSnackBar('Distribuir créditos', 'Créditos distribuidos com sucesso.');
          this.loadingService.dismiss();
          this.dialogRef.close(true);
        }, e => this.loadingService.dismiss());
    } else {
      this.dialogRef.close(false);
    }
  }

  countCreditos() {
    return this.data.plano.limiteOfertas - this.usuarios.reduce((a, b) => a += b.limiteOfertas, 0);
  }

  buscarUsuarios() {
    this.usuarioImobiliariaService.get(-99, -99, '1', 'ASC', this.data.id, 'C')
      .subscribe(res => this.usuarios = res.usuarios);
  }

}
