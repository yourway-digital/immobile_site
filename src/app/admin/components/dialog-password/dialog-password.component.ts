import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-dialog-password',
  templateUrl: './dialog-password.component.html',
  styleUrls: ['./dialog-password.component.scss']
})
export class DialogPasswordComponent implements OnInit {

  senha: string = '';
  confirmarSenha: string = '';

  constructor(
    public dialogRef: MatDialogRef<DialogPasswordComponent>,
    public helper: HelperService
  ) { }

  ngOnInit(): void {
  }

  submit(f: NgForm) {
    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      return;
    }
    if (this.senha !== this.confirmarSenha) {
      this.helper.openSnackBar('Alterar senha', 'As senhas precisam ser iguais.', true);
      return;
    }
    this.dismiss(true);
  }

  dismiss(status) {
    if (status) {
      this.dialogRef.close(this.senha);
    } else {
      this.dialogRef.close(false);
    }
  }
}
