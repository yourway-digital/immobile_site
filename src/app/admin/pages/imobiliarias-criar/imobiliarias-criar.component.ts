import { Imobiliaria } from 'src/app/models/imobiliaria.model';
import { ImobiliariaService } from 'src/app/services/admin/imobiliaria.service';
// Default
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatDialog } from '@angular/material/dialog';
import { UploadDialogComponent } from '../../components/upload-dialog/upload-dialog.component';
import { Plano } from 'src/app/models/plano.model';
import { PlanoService } from 'src/app/services/admin/plano.service';

@Component({
  selector: 'app-imobiliarias-criar',
  templateUrl: './imobiliarias-criar.component.html',
  styleUrls: ['./imobiliarias-criar.component.scss']
})
export class ImobiliariasCriarComponent implements OnInit {

  data: Imobiliaria = new Imobiliaria();
  phoneMask = '(00) 0000-00009';
  whatsappMask = '(00) 0000-00009';

  planos: Plano[] = [];

  constructor(
    public imobiliariaService: ImobiliariaService,
    public planoService: PlanoService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public global: GlobalService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.getPlanos();
  }

  getPlanos() {
    this.planoService.get(-99, -99, '1', 'ASC', 'A')
      .subscribe(res => this.planos = res.planos);
  }

  submit(f: NgForm) {

    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar('Formulário inválido', 'Preencha os campos requiridos, por favor!', true);
      return;
    }

    this.loadingService.present('Inserindo item...');

    this.imobiliariaService.post(this.data)
      .subscribe(res => {
        this.router.navigate(['/admin/imobiliarias']).then(() => {
          this.helper.openSnackBar('Inserção', 'Item inserido com sucesso.');
          this.loadingService.dismiss();
        })
      }, e => this.loadingService.dismiss());
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? this.data.situacao = 'A' : this.data.situacao = 'I';
  }

  openUploadDialog() {
    const dialogRef = this.dialog.open(UploadDialogComponent, {
      width: '640px',
      data: {
        imagePath: this.data.imagem,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) return;
      this.data.imagem = result;
    });
  }

  changePhoneMask(event, field) {
    let val = event;
    if (!val) {
      return;
    }
    if (val.replace(/\D/g, '').substring(0, 4) == "0800") {
      this[field] = '0000 000 0000';
    } else {
      if (val.replace(/\D/g, '').length === 11) {
        this[field] = '(00) 0 0000-0000';
      } else {
        this[field] = '(00) 0000-00009';
      }
    }
  }

  @ViewChild('enderecoNumeroInput', { static: false }) enderecoNumeroInput: ElementRef;
  onCepBlur(e) {
    if (this.data.cep.length == 8) {
      this.helper.viaCep(this.data.cep)
        .subscribe((res) => {
          this.data.bairro = res.bairro;
          this.data.uf = res.uf;
          this.data.cidade = res.localidade;
          this.data.endereco = res.logradouro;
          this.enderecoNumeroInput.nativeElement.focus();
        });
    }
  }

}
