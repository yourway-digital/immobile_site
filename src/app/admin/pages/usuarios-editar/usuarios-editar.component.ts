import { UsuarioService } from 'src/app/services/admin/usuario.service';
import { UsuarioAdmin } from 'src/app/models/usuario-admin.model';
// Default
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { DialogPasswordComponent } from '../../components/dialog-password/dialog-password.component';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-usuarios-editar',
  templateUrl: './usuarios-editar.component.html',
  styleUrls: ['./usuarios-editar.component.scss']
})
export class UsuariosEditarComponent implements OnInit {

  data: UsuarioAdmin = new UsuarioAdmin();
  hide: boolean = false;
  showPassword: boolean = false;

  constructor(
    public usuarioService: UsuarioService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public route: ActivatedRoute,
    public global: GlobalService,
    public dialog: MatDialog,
    public auth: AuthService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => this.buscar(params?.['id']));
  }

  buscar(id: number) {
    this.usuarioService.getById(id)
      .subscribe(res => this.data = res);
  }

  submit(f: NgForm) {

    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar('Formulário inválido', 'Preencha os campos requiridos, por favor!', true);
      return;
    }

    this.loadingService.present('Inserindo item...');

    this.usuarioService.patch(this.data)
      .subscribe((res: any) => {
        this.router.navigate(['/admin/usuarios']).then(() => {
          this.helper.openSnackBar('Inserção', 'Item inserido com sucesso.');
          this.loadingService.dismiss();
        })
      }, e => this.loadingService.dismiss());
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? this.data.situacao = 'A' : this.data.situacao = 'I';
  }

  openPasswordDialog() {
    const dialogRef = this.dialog.open(DialogPasswordComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(result);
        this.loadingService.present('Alterando senha...');
        this.usuarioService.changePassword(this.data.email, result)
          .subscribe(res => {
            if (this.data.id === this.auth.user.id) {
              const token = btoa(`${this.data.email}:${result}`);
              this.auth.token = token;
              this.auth.setToken(token);
            }
            this.helper.openSnackBar('Senha alterada com sucesso.', 'Alterar senha');
            this.loadingService.dismiss();
          }, e => this.loadingService.dismiss());
      }
    })
  }
}
