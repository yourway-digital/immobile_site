import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { ActivatedRoute, Router } from '@angular/router';
import { Plano } from 'src/app/models/plano.model';
import { PlanoService } from 'src/app/services/admin/plano.service';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-planos-editar',
  templateUrl: './planos-editar.component.html',
  styleUrls: ['./planos-editar.component.scss']
})
export class PlanosEditarComponent implements OnInit {

  data: Plano = new Plano();

  constructor(
    public planoService: PlanoService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public route: ActivatedRoute,
    public global: GlobalService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => this.buscar(params?.['id']));
  }

  buscar(id) {
    this.planoService.getById(id).subscribe(res => this.data = res);
  }

  submit(f: NgForm) {

    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar('Formulário inválido', 'Preencha os campos requiridos, por favor!', true);
      return;
    }

    this.loadingService.present('Alterando item...');

    this.planoService.patch(this.data)
      .subscribe((res: any) => {
        this.router.navigate(['/admin/planos']).then(() => {
          this.helper.openSnackBar('Alteração', 'Item alterado com sucesso.');
          this.loadingService.dismiss();
        })
      }, e => this.loadingService.dismiss());
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? this.data.situacao = 'A' : this.data.situacao = 'I';
  }

}
