import { UsuarioAppService, UsuariosApi } from 'src/app/services/admin/usuario-app.service';
import { UsuarioApp } from 'src/app/models/usuario-app.model';
// default
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../../components/dialog/dialog.component';

@Component({
  selector: 'app-usuarios-app',
  templateUrl: './usuarios-app.component.html',
  styleUrls: ['./usuarios-app.component.scss']
})
export class UsuariosAppComponent implements OnInit {

  displayedColumns: string[] = ['1', '2', '3', 'actions'];
  // nome, email, tipo, situacao
  data: UsuarioApp[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  search: string;
  filterSubscription: Subscription;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  situacao: string = '%';

  constructor(
    public global: GlobalService,
    public usuarioAppService: UsuarioAppService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public dialog: MatDialog,

  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.buscar();
  }

  buscar() {
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.usuarioAppService.get(this.paginator.pageIndex + 1, this.paginator.pageSize, this.sort.active, this.sort.direction.toLocaleUpperCase(), this.situacao, this.search);
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.numeroPaginas * this.paginator.pageSize;
          return data.usuarios;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.data = data;
      });
  }

  filter(e) {
    if (this.paginator.pageIndex > 1) {
      this.paginator.pageIndex = 0;
    }
    if (e) {
      if (this.filterSubscription && !this.filterSubscription.closed) {
        this.filterSubscription.unsubscribe();
      }
      this.filterSubscription = this.usuarioAppService.get(this.paginator.pageIndex + 1, this.paginator.pageSize, this.sort.active, this.sort.direction.toLocaleUpperCase(), this.situacao, e.toLocaleLowerCase())
        .subscribe((res: UsuariosApi) => {
          this.data = this.paginator.pageIndex == 0 ? res.usuarios : this.data.concat(res.usuarios);
        });
    } else {
      this.buscar();
    }
  }

  clearFilter() {
    this.search = '';
    this.buscar();
  }
}
