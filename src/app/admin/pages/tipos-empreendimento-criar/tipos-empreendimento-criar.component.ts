import { TipoEmpreendimentoService } from 'src/app/services/admin/tipo-empreendimento.service';
import { TipoEmpreendimento } from 'src/app/models/tipo-empreendimento.model';
// Default
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatDialog } from '@angular/material/dialog';
import { UploadDialogComponent } from '../../components/upload-dialog/upload-dialog.component';

@Component({
  selector: 'app-tipos-empreendimento-criar',
  templateUrl: './tipos-empreendimento-criar.component.html',
  styleUrls: ['./tipos-empreendimento-criar.component.scss']
})
export class TiposEmpreendimentoCriarComponent implements OnInit {

  data: TipoEmpreendimento = new TipoEmpreendimento();

  constructor(
    public tipoEmpreendimentoService: TipoEmpreendimentoService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public global: GlobalService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
  }

  submit(f: NgForm) {

    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar('Formulário inválido', 'Preencha os campos requiridos, por favor!', true);
      return;
    }

    this.loadingService.present('Inserindo item...');

    this.tipoEmpreendimentoService.post(this.data)
      .subscribe(res => {
        this.router.navigate(['/admin/tipos-empreendimento']).then(() => {
          this.helper.openSnackBar('Inserção', 'Item inserido com sucesso.');
          this.loadingService.dismiss();
        })
      }, e => this.loadingService.dismiss());
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? this.data.situacao = 'A' : this.data.situacao = 'I';
  }

  openUploadDialog() {
    const dialogRef = this.dialog.open(UploadDialogComponent, {
      width: '640px',
      data: {
        imagePath: this.data.icone,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) return;
      this.data.icone = result;
    });
  }

}
