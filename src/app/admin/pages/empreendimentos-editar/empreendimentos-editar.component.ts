import { EmpreendimentoService } from 'src/app/services/admin/empreendimento.service';
import { Empreendimento } from 'src/app/models/empreendimento.model';
// Default
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import {
  empreendimentoStatus,
  empreendimentoTiposOferta,
  GlobalService,
} from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { SubtipoEmpreendimento } from 'src/app/models/subtipo-empreendedorismo.model';
import { TipoEmpreendimento } from 'src/app/models/tipo-empreendimento.model';
import { CidadeProps, EstadoProps, LocalizacaoService } from 'src/app/services/admin/localizacao.service';
import { TipoEmpreendimentoService } from 'src/app/services/admin/tipo-empreendimento.service';
import { SubtipoEmpreendimentoService } from 'src/app/services/admin/subtipo-empreendimento.service';
import { ImobiliariaService } from 'src/app/services/admin/imobiliaria.service';
import { Imobiliaria } from 'src/app/models/imobiliaria.model';
import { EmpreendimentoImagem } from 'src/app/models/empreendimento-imagem.model';
import { EmpreendimentoImagemService } from 'src/app/services/admin/empreendimento-imagem.service';
import { UploadService } from 'src/app/services/upload.service';
import { HttpProgressEvent } from '@angular/common/http';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { DialogComponent } from '../../components/dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioImobiliariaService } from 'src/app/services/admin/usuario-imobiliaria.service';
import { UsuarioImobiliaria } from 'src/app/models/usuario-imobiliaria.model';
import {
  GridLayout,
  Image,
  PlainGalleryConfig,
  PlainGalleryStrategy,
} from '@ks89/angular-modal-gallery';
import { ModalGalleryComponent } from '@ks89/angular-modal-gallery/lib/components/components';

@Component({
  selector: 'app-empreendimentos-editar',
  templateUrl: './empreendimentos-editar.component.html',
  styleUrls: ['./empreendimentos-editar.component.scss'],
})
export class EmpreendimentosEditarComponent implements OnInit {
  @ViewChild('modalGallery') modalGallery: ModalGalleryComponent;

  data: Empreendimento = new Empreendimento();

  imobiliarias: Imobiliaria[] = [];
  searchImobiliarias: string = '';

  estados: EstadoProps[] = [];
  searchEstados: string = '';

  cidades: CidadeProps[] = [];
  searchCidades: string = '';

  subtiposEmpreendimento: SubtipoEmpreendimento[] = [];
  searchSubtiposEmpreendimento: string = '';

  tiposEmpreendimento: TipoEmpreendimento[] = [];
  searchTiposEmpreendimento: string = '';

  searchUsuariosImobiliaria: string = '';
  usuariosImobiliaria: UsuarioImobiliaria[] = [];

  files: File[] = [];
  progress: number = 0;

  imagens: Image[] = [];
  plainGalleryGrid: PlainGalleryConfig = {
    strategy: PlainGalleryStrategy.GRID,
    layout: new GridLayout(
      { width: '0px', height: '0px' },
      { length: 10, wrap: true }
    ),
  };

  empreendimentoStatus = empreendimentoStatus;
  empreendimentoTiposOferta = empreendimentoTiposOferta;

  constructor(
    public empreendimentoService: EmpreendimentoService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public route: ActivatedRoute,
    public global: GlobalService,
    public localizacaoService: LocalizacaoService,
    public tipoEmpreendimentoService: TipoEmpreendimentoService,
    public subtipoEmpreendimentoService: SubtipoEmpreendimentoService,
    public usuarioImobiliariaService: UsuarioImobiliariaService,
    public imobiliariaService: ImobiliariaService,
    public empreendimentoImagemService: EmpreendimentoImagemService,
    public uploadService: UploadService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.route.params.subscribe((param) => this.buscar(param?.['id']));
    this.buscarEstados();
    this.buscarImobiliarias();
    this.buscarTiposEmpreendimento();
  }

  buscar(id: number) {
    this.empreendimentoService.getById(id).subscribe((res) => {
      this.data = res;
      if (res.uf) {
        this.buscarCidades(res.uf);
      }
      if (res.tipoEmpreendimento.id) {
        this.buscarSubtiposEmpreendimento(res.tipoEmpreendimento.id);
      }
      if (res.imobiliaria.id) {
        this.buscarUsuariosImobiliaria(res.imobiliaria.id);
      }
      if (res.imagens.length) {
        this.imagens = res.imagens.map((item) => {
          const obj = {
            id: item.id,
            modal: {
              alt: item.nome,
              ariaLabel: item.nome,
              downloadFileName: item.nome,
              extUrl: this.global.imageUrl + item.imagem,
              img: this.global.imageUrl + item.imagem,
              title: item.nome,
            },
          } as Image;
          return obj;
        });
      }
    });
  }

  submit(f: NgForm) {
    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar(
        'Formulário inválido',
        'Preencha os campos requiridos, por favor!',
        true
      );
      return;
    }

    this.loadingService.present('Alterando item...');

    this.empreendimentoService.patch(this.data).subscribe(
      (res: any) => {
        this.router.navigate(['/admin/empreendimentos']).then(() => {
          this.helper.openSnackBar('Alteração', 'Item alterado com sucesso.');
          this.loadingService.dismiss();
        });
      },
      (e) => this.loadingService.dismiss()
    );
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? (this.data.situacao = 'A') : (this.data.situacao = 'I');
  }

  buscarEstados() {
    this.localizacaoService.getEstados().subscribe((res) => {
      this.estados = res.map((e) => {
        e.descricao = `${e.sigla} - ${e.nome}`;
        return e;
      });
    });
  }

  buscarCidades(siglaUf) {
    this.localizacaoService
      .getCidades(siglaUf)
      .subscribe((res: CidadeProps[]) => (this.cidades = res));
  }

  buscarTiposEmpreendimento() {
    this.tipoEmpreendimentoService
      .get(-99, -99, '1', 'ASC', 'A')
      .subscribe((res) => (this.tiposEmpreendimento = res.tiposEmpreendimento));
  }

  buscarSubtiposEmpreendimento(idTipoEmpreendimento) {
    this.subtipoEmpreendimentoService
      .get(idTipoEmpreendimento)
      .subscribe((res) => (this.subtiposEmpreendimento = res));
  }

  buscarImobiliarias() {
    this.imobiliariaService
      .get(-99, -99, '1', 'ASC', 'A')
      .subscribe((res) => (this.imobiliarias = res.imobiliarias));
  }

  buscarUsuariosImobiliaria(idImobiliaria) {
    this.usuarioImobiliariaService
      .get(-99, -99, '1', 'ASC', idImobiliaria, 'C', 'A')
      .subscribe((res) => (this.usuariosImobiliaria = res.usuarios));
  }

  onChangeEstado(e) {
    if (!e) {
      this.cidades = [];
      return;
    }
    this.buscarCidades(e);
  }

  onChangeTipoEmpreendimento(e) {
    if (!e) {
      this.subtiposEmpreendimento = [];
      return;
    }
    this.buscarSubtiposEmpreendimento(e.id);
  }

  onImobiliariaChange(e) {
    if (!e) {
      this.usuariosImobiliaria = [];
      return;
    }
    this.buscarUsuariosImobiliaria(e.id);
  }

  @ViewChild('numeroEnderecoInput') numeroEnderecoInputRef: ElementRef;
  onBlurCep(e) {
    const cep = e.target.value.replace('-', '');
    if (cep.length < 8) return;
    this.data.latitude = this.data.latitude ? this.data.latitude : 0;
    this.data.longitude = this.data.longitude ? this.data.longitude : 0;
    this.helper.viaCep(cep).subscribe((res) => {
      if (res.uf) {
        this.buscarCidades(res.uf);
      }
      (this.data.uf = res.uf),
        (this.data.cidade = res.localidade),
        (this.data.bairro = res.bairro),
        (this.data.endereco = res.logradouro),
        this.numeroEnderecoInputRef.nativeElement.focus();
    });
    this.googleGeocode(cep);
  }

  onSelect(event) {
    this.files.push(...event.addedFiles);

    this.loadingService.present('Inserindo...');

    this.uploadService
      .uploadMultipleFiles(
        this.files,
        `/gerenciador/empreendimentoImagem/inserirMultiplo?idEmpreendimento=${this.data.id}`
      )
      .subscribe(
        (event: HttpProgressEvent | any) => {
          if (event.type === 4) {
            this.progress = 0;
            this.files = [];
            this.buscarImagens();
            this.loadingService.dismiss();
          } else {
            this.progress = Math.round((event.loaded / event.total) * 100);
            if (isNaN(this.progress)) {
              this.progress = 100;
            }
            this.loadingService.title = `${this.progress}%`;
          }
        },
        (e) => this.loadingService.dismiss()
      );
  }

  onRemove(event) {
    this.files.splice(this.files.indexOf(event), 1);
  }

  buscarImagens() {
    this.empreendimentoImagemService
      .get(this.data.id)
      .subscribe((res) => (this.data.imagens = res));
  }

  removeImage(imagem: EmpreendimentoImagem) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {
        title: 'Excluir imagem',
        description:
          'Você realmente quer excluir essa imagem? Esse processe não pode ser desfeito.',
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.loadingService.present('Removendo...');
        this.empreendimentoImagemService.delete(imagem.id).subscribe(
          (res) => {
            this.buscarImagens();
            this.loadingService.dismiss();
          },
          (e) => this.loadingService.dismiss()
        );
      }
    });
  }

  starImage(imagem: EmpreendimentoImagem) {
    this.loadingService.present('Favoritando...');
    this.empreendimentoImagemService.setPrincipal(imagem.id).subscribe(
      (res) => {
        this.buscarImagens();
        this.loadingService.dismiss();
      },
      (e) => this.loadingService.dismiss()
    );
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.data.imagens, event.previousIndex, event.currentIndex);

    this.data.imagens = this.data.imagens.map((image, index) => {
      image.ordem = index + 1;
      return image;
    });

    this.loadingService.present('Ordenando');

    this.empreendimentoImagemService.order(this.data.imagens).subscribe(
      (res) => this.loadingService.dismiss(),
      (e) => this.loadingService.dismiss()
    );
  }

  googleGeocode(cep: string) {
    const API_KEY = 'AIzaSyBNxHqA_kCOuCMkPpomr1bWPN5uTlgnIgU';
    const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${cep}&key=${API_KEY}`;
    fetch(url, {
      method: 'GET',
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.status === 'OK') {
          const result = res.results[0];
          if (result) {
            this.data.latitude = result.geometry.location.lat;
            this.data.longitude = result.geometry.location.lng;
          }
        }
      });
  }

  getLatLng(e) {
    this.data.latitude = e.lat;
    this.data.longitude = e.lng;
  }

  onVisibleIndex(event) {
    console.log(event);
  }

  onNumeroEnderecoBlur(e) {
    if (
      !this.data.endereco ||
      !this.data.bairro ||
      !this.data.cidade ||
      !this.data.uf
    ) {
      return;
    }

    const field = `${this.data.numeroEndereco}+${this.data.endereco}+${this.data.bairro}+${this.data.cidade}+${this.data.uf}`;
    this.googleGeocode(field);
  }
}
