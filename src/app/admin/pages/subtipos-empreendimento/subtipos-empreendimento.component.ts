// default
import { SelectionModel } from '@angular/cdk/collections';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { DialogComponent } from 'src/app/admin/components/dialog/dialog.component';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { SubtipoEmpreendimentoService } from 'src/app/services/admin/subtipo-empreendimento.service';
import { SubtipoEmpreendimento } from 'src/app/models/subtipo-empreendedorismo.model';
import { SubtipoDialogComponent } from '../../components/subtipo-dialog/subtipo-dialog.component';

@Component({
  selector: 'app-subtipos-empreendimento',
  templateUrl: './subtipos-empreendimento.component.html',
  styleUrls: ['./subtipos-empreendimento.component.scss']
})
export class SubtiposEmpreendimentoComponent implements OnInit {

  @Input('idTipoEmpreendimento') idTipoEmpreendimento: number;

  data: SubtipoEmpreendimento[] = [];
  displayedColumns: string[] = ['select', '1', '2', 'actions'];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  selection = new SelectionModel<SubtipoEmpreendimento>(true, []);

  situacao: string = '%';

  constructor(
    public global: GlobalService,
    public subtipoEmpreendimentoService: SubtipoEmpreendimentoService,
    public dialog: MatDialog,
    public helper: HelperService,
    public loadingService: LoadingService
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.buscar();
  }

  buscar() {
    merge(this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.subtipoEmpreendimentoService.get(this.idTipoEmpreendimento, this.situacao);
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.length;
          this.selection.clear();
          return data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          this.selection.clear();
          return observableOf([]);
        })
      ).subscribe(data => this.data = data);
  }

  deletarData(item: SubtipoEmpreendimento) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {
        title: 'Excluir item',
        description: 'Você realmente quer excluir esse item? Esse processe não pode ser desfeito.'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loadingService.present('Excluindo item...');
        this.subtipoEmpreendimentoService.delete(item)
          .subscribe(res => {
            this.helper.openSnackBar('Remoção', 'Item removido com sucesso.');
            this.loadingService.dismiss();
            this.buscar();
          }, e => this.loadingService.dismiss());
      }
    })
  }

  deletarDatas() {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {
        title: 'Excluir itens',
        description: 'Você realmente quer excluir esses itens? Esse processe não pode ser desfeito.'
      }
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {
        this.subtipoEmpreendimentoService.deleteSelected(this.selection.selected).subscribe((res: any) => {
          this.loadingService.present('Excluindo itens');
          this.helper.openSnackBar('Remoção', 'Itens removidos com sucesso.');
          this.loadingService.dismiss();
          this.buscar();
        }, e => this.loadingService.dismiss());
      }
    })
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ? this.selection.clear() : this.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  openSubtipoDialog(data?: SubtipoEmpreendimento) {
    const dialogRef = this.dialog.open(SubtipoDialogComponent, {
      width: '400px',
      data: data ? data : new SubtipoEmpreendimento()
    });

    dialogRef.afterClosed()
      .subscribe(result => {

        if (!result) {
          return;
        }

        this.loadingService.present(result.id ? 'Alterando item...' : 'Inserindo item...');

        if (result.id) {
          this.subtipoEmpreendimentoService.patch(result)
            .subscribe(res => {
              this.helper.openSnackBar('Item editado com sucesso.');
              this.loadingService.dismiss();
              this.buscar();
            }, e => this.loadingService.dismiss());
        } else {
          const data = {
            ...result,
            idTipoEmpreendimento: this.idTipoEmpreendimento,
          } as SubtipoEmpreendimento
          this.subtipoEmpreendimentoService.post(data)
            .subscribe(res => {
              this.helper.openSnackBar('Inserção', 'Item inserido com sucesso.');
              this.loadingService.dismiss();
              this.buscar();
            }, e => this.loadingService.dismiss());
        }
      })
  }
}
