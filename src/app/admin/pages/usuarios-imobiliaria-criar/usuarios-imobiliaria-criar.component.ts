import { Imobiliaria } from 'src/app/models/imobiliaria.model';
import { ImobiliariaService } from 'src/app/services/admin/imobiliaria.service';
import { UsuarioImobiliaria } from 'src/app/models/usuario-imobiliaria.model';
import { UsuarioImobiliariaService } from 'src/app/services/admin/usuario-imobiliaria.service';
// default
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-usuarios-imobiliaria-criar',
  templateUrl: './usuarios-imobiliaria-criar.component.html',
  styleUrls: ['./usuarios-imobiliaria-criar.component.scss'],
})
export class UsuariosImobiliariaCriarComponent implements OnInit {
  data: UsuarioImobiliaria = new UsuarioImobiliaria();

  searchImobiliarias: string = '';
  imobiliarias: Imobiliaria[] = [];
  loadingImobiliarias: boolean = false;

  phoneMask: string = '(00) 0000-00009';

  constructor(
    public usuarioImobiliariaService: UsuarioImobiliariaService,
    public imobiliariaService: ImobiliariaService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public global: GlobalService,
    public auth: AuthService
  ) {}

  ngOnInit() {
    this.buscarImobiliarias();
  }

  buscarImobiliarias() {
    this.loadingImobiliarias = true;
    this.imobiliariaService.get(-99, -99, '0', 'ASC', 'A').subscribe(
      (res) => {
        this.imobiliarias = res.imobiliarias;
        setTimeout(() => (this.loadingImobiliarias = false), 500);
      },
      (e) => (this.loadingImobiliarias = false)
    );
  }

  submit(f: NgForm) {
    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar(
        'Formulário inválido',
        'Preencha os campos requiridos, por favor!',
        true
      );
      return;
    }

    if (this.data?.senha !== this.data?.re_senha) {
      this.helper.openSnackBar(
        'Inserir usuário',
        'As senhas precisam ser iguais.',
        true
      );
      return;
    }

    this.loadingService.present('Inserindo item...');

    const user = {
      ...this.data,
    } as UsuarioImobiliaria;
    delete user['re_senha'];

    this.usuarioImobiliariaService.post(user).subscribe(
      (res: any) => {
        this.router.navigate(['/admin/usuarios-imobiliaria']).then(() => {
          this.helper.openSnackBar('Inserção', 'Item inserido com sucesso.');
          this.loadingService.dismiss();
        });
      },
      (e) => this.loadingService.dismiss()
    );
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? (this.data.situacao = 'A') : (this.data.situacao = 'I');
  }

  onImobiliariaOpen(event) {
    if (event) {
      this.buscarImobiliarias();
    }
  }

  changePhoneMask(event) {
    let val = event;
    if (!val) {
      return;
    }
    if (val.replace(/\D/g, '').substring(0, 4) == '0800') {
      this.phoneMask = '0000 000 0000';
    } else {
      if (val.replace(/\D/g, '').length === 11) {
        this.phoneMask = '(00) 0 0000-0000';
      } else {
        this.phoneMask = '(00) 0000-00009';
      }
    }
  }
}
