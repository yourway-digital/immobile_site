import { Empreendimento } from 'src/app/models/empreendimento.model';
import {
  EmpreendimentosApi,
  EmpreendimentoService,
} from 'src/app/services/admin/empreendimento.service';
// default
import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { DialogComponent } from 'src/app/admin/components/dialog/dialog.component';
import {
  empreendimentoStatus,
  empreendimentoTiposOferta,
  GlobalService,
} from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ImobiliariaService } from 'src/app/services/admin/imobiliaria.service';
import { Imobiliaria } from 'src/app/models/imobiliaria.model';
import { TipoEmpreendimentoService } from 'src/app/services/admin/tipo-empreendimento.service';
import { TipoEmpreendimento } from 'src/app/models/tipo-empreendimento.model';

@Component({
  selector: 'app-empreendimentos',
  templateUrl: './empreendimentos.component.html',
  styleUrls: ['./empreendimentos.component.scss'],
})
export class EmpreendimentosComponent implements OnInit {
  displayedColumns: string[] = [
    'select',
    'imagem',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    'actions',
  ];
  // imagem, nome, endereco, valor, imobiliaria, tipoEmpreendimento, status, tipo da oferta, situacao
  data: Empreendimento[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  idImobiliaria: number = 0;
  searchImobiliarias: string = '';
  imobiliarias: Imobiliaria[] = [];

  idTipoEmpreendimento: number = 0;
  searchTiposEmpreendimento: string = '';
  tiposEmpreendimento: TipoEmpreendimento[] = [];

  status: string = '';
  tipoOferta: string = '';
  situacao: string = '';
  search: string = '';

  filterSubscription: Subscription;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  selection = new SelectionModel<Empreendimento>(true, []);

  empreendimentoStatus = empreendimentoStatus;
  empreendimentoTiposOferta = empreendimentoTiposOferta;

  constructor(
    public global: GlobalService,
    public empreendimentoService: EmpreendimentoService,
    public imobiliariaService: ImobiliariaService,
    public tipoEmpreendimentoService: TipoEmpreendimentoService,
    public dialog: MatDialog,
    public helper: HelperService,
    public loadingService: LoadingService
  ) {}

  ngOnInit() {
    this.buscarImobiliarias();
    this.buscarTiposEmpreendimentos();
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    this.buscar();
  }

  buscarImobiliarias() {
    this.imobiliariaService
      .get(-99, -99, '1', 'ASC', 'A')
      .subscribe((res) => (this.imobiliarias = res.imobiliarias));
  }

  buscarTiposEmpreendimentos() {
    this.tipoEmpreendimentoService
      .get(-99, -99, '1', 'ASC', 'A')
      .subscribe((res) => (this.tiposEmpreendimento = res.tiposEmpreendimento));
  }

  buscar() {
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.empreendimentoService.get(
            this.paginator.pageIndex + 1,
            this.paginator.pageSize,
            this.sort.active,
            this.sort.direction.toLocaleUpperCase(),
            this.idImobiliaria,
            this.status,
            this.tipoOferta,
            this.idTipoEmpreendimento,
            '%',
            this.situacao,
            this.search, 
            false
          );
        }),
        map((data) => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.selection.clear();
          this.resultsLength = data.numeroPaginas * this.paginator.pageSize;
          return data.empreendimentos;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          this.selection.clear();
          return observableOf([]);
        })
      )
      .subscribe((data) => {
        this.data = data;
      });
  }

  deletarData(item: Empreendimento) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {
        title: 'Excluir item',
        description:
          'Você realmente quer excluir esse item? Esse processe não pode ser desfeito.',
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.loadingService.present('Excluindo item...');
        this.empreendimentoService.delete(item).subscribe(
          (res) => {
            this.helper.openSnackBar('Remoção', 'Item removido com sucesso.');
            this.loadingService.dismiss();
            this.buscar();
          },
          (e) => this.loadingService.dismiss()
        );
      }
    });
  }

  deletarDatas() {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {
        title: 'Excluir itens',
        description:
          'Você realmente quer excluir esses itens? Esse processe não pode ser desfeito.',
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.empreendimentoService
          .deleteSelected(this.selection.selected)
          .subscribe(
            (res: any) => {
              this.loadingService.present('Excluindo itens');
              this.helper.openSnackBar(
                'Remoção',
                'Itens removidos com sucesso.'
              );
              this.loadingService.dismiss();
              this.buscar();
            },
            (e) => this.loadingService.dismiss()
          );
      }
    });
  }

  sendNotification(data: Empreendimento) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {
        title: 'Enviar notificação',
        description: 'Você realmente deseja realmente enviar essa notificação?',
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.loadingService.present('Enviando notificação');
        this.empreendimentoService.sendExpoNotification(data?.id).subscribe({
          next: (res) => {
            this.loadingService.dismiss();
          },
          error: () => this.loadingService.dismiss(),
        });
      }
    });
  }

  filter(e) {
    if (this.paginator.pageIndex > 1) {
      this.paginator.pageIndex = 0;
    }
    if (e) {
      if (this.filterSubscription && !this.filterSubscription.closed) {
        this.filterSubscription.unsubscribe();
      }
      this.filterSubscription = this.empreendimentoService
        .get(
          this.paginator.pageIndex + 1,
          this.paginator.pageSize,
          this.sort.active,
          this.sort.direction.toLocaleUpperCase(),
          this.idImobiliaria,
          this.status,
          this.tipoOferta,
          this.idTipoEmpreendimento,
          '%',
          this.situacao,
          this.search, 
          false
        )
        .subscribe((res: EmpreendimentosApi) => {
          this.data =
            this.paginator.pageIndex == 0
              ? res.empreendimentos
              : this.data.concat(res.empreendimentos);
        });
    } else {
      this.buscar();
    }
  }

  clearFilter() {
    this.search = '';
    this.buscar();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.data.forEach((row) => this.selection.select(row));
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.position + 1
    }`;
  }

  onStatusChange(e) {
    this.buscar();
  }

  onTipoOfertaChange(e) {
    this.buscar();
  }

  onSituacaoChange(e) {
    this.buscar();
  }

  onImobiliariaChange(e) {
    this.buscar();
  }

  onTipoEmpreendimentoChange(e) {
    this.buscar();
  }

  generateFile() {
    this.loadingService.present('Gerando arquivo...')
    this.empreendimentoService.generateFacebookFile().subscribe(
      (link) => {
        // window.open(link, '_blank');
        const fileName = `immobile_fb_${this.helper
          .moment()
          .format('DD_MM_YYYY_HH_mm')}`
        this.helper.download(fileName, link)
        this.loadingService.dismiss()
      },
      (e) => this.loadingService.dismiss()
    )
  }
}
