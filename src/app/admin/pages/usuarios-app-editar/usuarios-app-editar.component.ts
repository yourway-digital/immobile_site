import { Component, OnInit } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioApp } from 'src/app/models/usuario-app.model';
import { UsuarioAppService } from 'src/app/services/admin/usuario-app.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-usuarios-app-editar',
  templateUrl: './usuarios-app-editar.component.html',
  styleUrls: ['./usuarios-app-editar.component.scss']
})
export class UsuariosAppEditarComponent implements OnInit {

  data: UsuarioApp = new UsuarioApp();
  hide: boolean = false;

  showPassword: boolean = false;

  constructor(
    public usuarioAppService: UsuarioAppService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => this.buscar(params?.['id']));
  }

  buscar(id: number) {
    this.usuarioAppService.getById(id)
      .subscribe(res => this.data = res);
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? this.data.situacao = 'A' : this.data.situacao = 'I';
  }

}
