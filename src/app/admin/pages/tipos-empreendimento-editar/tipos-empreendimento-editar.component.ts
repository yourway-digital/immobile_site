import { TipoEmpreendimentoService } from 'src/app/services/admin/tipo-empreendimento.service';
import { TipoEmpreendimento } from 'src/app/models/tipo-empreendimento.model';
// Default
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatDialog } from '@angular/material/dialog';
import { UploadDialogComponent } from '../../components/upload-dialog/upload-dialog.component';

@Component({
  selector: 'app-tipos-empreendimento-editar',
  templateUrl: './tipos-empreendimento-editar.component.html',
  styleUrls: ['./tipos-empreendimento-editar.component.scss'],
})
export class TiposEmpreendimentoEditarComponent implements OnInit {
  data: TipoEmpreendimento = new TipoEmpreendimento();

  constructor(
    public tipoEmpreendimentoService: TipoEmpreendimentoService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public route: ActivatedRoute,
    public global: GlobalService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => this.buscar(params?.['id']));
  }

  buscar(id) {
    this.tipoEmpreendimentoService
      .getById(id)
      .subscribe((res) => (this.data = res));
  }

  submit(f: NgForm) {
    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar(
        'Formulário inválido',
        'Preencha os campos requiridos, por favor!',
        true
      );
      return;
    }

    this.loadingService.present('Alterando item...');

    this.tipoEmpreendimentoService.patch(this.data).subscribe(
      (res) => {
        this.router.navigate(['/admin/tipos-empreendimento']).then(() => {
          this.helper.openSnackBar('Alteração', 'Item alterado com sucesso.');
          this.loadingService.dismiss();
        });
      },
      (e) => this.loadingService.dismiss()
    );
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? (this.data.situacao = 'A') : (this.data.situacao = 'I');
  }

  openUploadDialog() {
    const dialogRef = this.dialog.open(UploadDialogComponent, {
      width: '640px',
      data: {
        imagePath: this.data.imagem,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (!result) return;
      this.data.icone = result;
    });
  }
}
