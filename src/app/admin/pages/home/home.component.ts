import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { EstatisticaService } from 'src/app/services/admin/estatistica.service';
import { AuthService } from 'src/app/services/auth.service';

import { Chart, registerables } from 'chart.js';
import {
  DashboardService,
  DashboardType,
  LocationType,
} from 'src/app/services/admin/dashboard.service';
import { HelperService } from 'src/app/services/helper.service';
import { forkJoin } from 'rxjs';
Chart.register(...registerables);

interface HomeStats {
  corretores: number;
  empreendimentos: number;
  imobiliarias: number;
  usuariosApp: number;
  activeUsers: number;
  inactiveUsers: number;
}

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, AfterViewInit {
  pages = [];

  homeStats: HomeStats = {
    corretores: 0,
    empreendimentos: 0,
    imobiliarias: 0,
    usuariosApp: 0,
    activeUsers: 0,
    inactiveUsers: 0,
  };

  labels = [];
  datapoints = [1, 20, 20, 60, 60, 120, NaN, 180, 120, 125, 105, 110, 170];
  data: any = {};
  config: any = {};

  tipoData: string = 'D';
  dataInicial: Date = new Date();
  dataFinal: Date = new Date();

  tipoLocalizacao: LocationType = 'C';

  chartViews: Chart;
  chartClicks: Chart;
  chartLeads: Chart;
  chartLocation: Chart;
  chartAppAccess: Chart;

  constructor(
    public _auth: AuthService,
    private cdref: ChangeDetectorRef,
    public estatisticaService: EstatisticaService,
    public dashboardService: DashboardService,
    private helper: HelperService
  ) {
    this.dataInicial = this.helper.moment().subtract('30', 'days').toDate();
  }

  ngOnInit() {
    this.buildPages();
    this.buscarDadosContadores();
  }

  ngAfterViewInit() {
    this.cdref.detectChanges();

    this.getLocationData();
    // this.getData('APARICOES');
    this.getData('CLICKS');
    this.getData('CONTATOS');
    this.getData('ACESSOAPP');
  }

  getData(tipoEstatistica: DashboardType) {
    if (this.tipoData === 'D') {
      this.dashboardService
        .getByDay(
          tipoEstatistica,
          this.helper.moment(this.dataInicial).format('YYYY-MM-DD'),
          this.helper.moment(this.dataFinal).format('YYYY-MM-DD')
        )
        .subscribe((res) => this.initAllCharts(tipoEstatistica, res));
    } else if (this.tipoData === 'M') {
      this.dashboardService
        .getByMonth(
          tipoEstatistica,
          this.helper.moment(this.dataInicial).format('YYYY-MM-DD'),
          this.helper.moment(this.dataFinal).format('YYYY-MM-DD')
        )
        .subscribe((res) => this.initAllCharts(tipoEstatistica, res));
    } else {
      this.dashboardService
        .getByYear(
          tipoEstatistica,
          this.helper.moment(this.dataInicial).format('YYYY-MM-DD'),
          this.helper.moment(this.dataFinal).format('YYYY-MM-DD')
        )
        .subscribe((res) => this.initAllCharts(tipoEstatistica, res));
    }
  }

  getLocationData() {
    this.dashboardService
      .getByLocation(this.tipoLocalizacao)
      .subscribe((res) => {
        console.log(res);
        this.initLocationChart(
          'locationChart',
          res,
          'Empreendimento por localização'
        );
      });
  }

  initAllCharts(tipoEstatistica: DashboardType, data) {
    // if (tipoEstatistica === 'APARICOES') {
    //   this.initChart('viewsChart', data, 'Aparições');
    // }
    if (tipoEstatistica === 'CLICKS') {
      this.initChart('clicksChart', data, 'Clicks');
    }
    if (tipoEstatistica === 'CONTATOS') {
      this.initChart('leadsChart', data, 'Contatos');
    }
    if (tipoEstatistica === 'ACESSOAPP') {
      this.initChart('appAccessChart', data, 'Acesso app');
    }
  }

  initChart(chartId: string, chartData, title?) {
    const labels = chartData.map((item) => item.campo1);
    const data = chartData.map((item) => parseInt(item.campo2));

    const dataConfig = {
      labels: labels,
      datasets: [
        {
          label: title,
          backgroundColor: '#1D2545',
          borderColor: '#1D2545',
          data: data,
        },
      ],
    };

    const config = {
      type: 'line',
      data: dataConfig,
      options: {},
    };

    if (chartId === 'viewsChart' && !this.chartViews) {
      this.chartViews = new Chart(
        document.getElementById(chartId) as any,
        config as any
      );
      this.chartViews.render();
    } else if (chartId === 'viewsChart' && this.chartViews) {
      this.chartViews.data.labels = labels;
      this.chartViews.data.datasets[0].data = data;
      this.chartViews.update();
    }

    if (chartId === 'leadsChart' && !this.chartLeads) {
      this.chartLeads = new Chart(
        document.getElementById(chartId) as any,
        config as any
      );
      this.chartLeads.render();
    } else if (chartId === 'leadsChart' && this.chartLeads) {
      this.chartLeads.data.labels = labels;
      this.chartLeads.data.datasets[0].data = data;
      this.chartLeads.update();
    }

    if (chartId === 'clicksChart' && !this.chartClicks) {
      this.chartClicks = new Chart(
        document.getElementById(chartId) as any,
        config as any
      );
      this.chartClicks.render();
    } else if (chartId === 'clicksChart' && this.chartClicks) {
      this.chartClicks.data.labels = labels;
      this.chartClicks.data.datasets[0].data = data;
      this.chartClicks.update();
    }

    if (chartId === 'appAccessChart' && !this.chartAppAccess) {
      this.chartAppAccess = new Chart(
        document.getElementById(chartId) as any,
        config as any
      );
      this.chartAppAccess.render();
    } else if (chartId === 'appAccessChart' && this.chartAppAccess) {
      this.chartAppAccess.data.labels = labels;
      this.chartAppAccess.data.datasets[0].data = data;
      this.chartAppAccess.update();
    }
  }

  initLocationChart(chartId: string, chartData, title?) {
    const labels = chartData.map((item) => item.campo1);
    const data = chartData.map((item) => parseInt(item.campo2));

    const dataConfig = {
      labels: labels,
      datasets: [
        {
          label: title,
          backgroundColor: '#1D2545',
          borderColor: '#1D2545',
          data: data,
        },
      ],
    };

    const config = {
      type: 'bar',
      data: dataConfig,
      options: {
        indexAxis: 'y',
        elements: {
          bar: {
            borderWidth: 1,
          },
        },
        responsive: true,
        plugins: {
          legend: {
            position: 'right',
          },
          title: {
            display: true,
            text: title,
          },
        },
      },
    };

    if (chartId === 'locationChart' && !this.chartLocation) {
      this.chartLocation = new Chart(
        document.getElementById(chartId) as any,
        config as any
      );
      this.chartLocation.render();
    } else if (chartId === 'locationChart' && this.chartLocation) {
      this.chartLocation.data.labels = labels;
      this.chartLocation.data.datasets[0].data = data;
      this.chartLocation.update();
    }
  }

  buscarDadosContadores() {
    forkJoin([
      this.estatisticaService.getAccountantsAdmin(null, null),
      this.estatisticaService.getCountActiveUsers(),
      this.estatisticaService.getCountInactiveUsers(),
    ]).subscribe((res: any) => {
      this.homeStats = {
        corretores: res?.[0]?.corretores,
        empreendimentos: res?.[0]?.empreendimentos,
        imobiliarias: res?.[0]?.imobiliarias,
        usuariosApp: res?.[0]?.usuariosApp,
        activeUsers: res?.[1],
        inactiveUsers: res?.[2],
      };
    });
  }

  buildPages() {
    this.pages = [
      { name: 'Home', icon: 'fas fa-home', route: '/admin' },
      {
        name: 'Cadastros',
        icon: 'fas fa-list-ul',
        group: [
          {
            name: 'Tipos de Empreendimento',
            icon: 'fas fa-border-style',
            route: '/admin/tipos-empreendimento',
          },
          {
            name: 'Imobiliárias',
            icon: 'fas fa-store-alt',
            route: '/admin/imobiliarias',
          },
          {
            name: 'Empreendimentos',
            icon: 'fas fa-building',
            route: '/admin/empreendimentos',
          },
          { name: 'Planos', icon: 'fas fa-tags', route: '/admin/planos' },
        ],
        openGroup: false,
      },
      {
        name: 'Usuários',
        icon: 'fas fa-users',
        group: [
          {
            name: 'Aplicativo',
            icon: 'fas fa-user-friends',
            route: '/admin/usuarios-app',
          },
          {
            name: 'Imobiliárias',
            icon: 'fas fa-user-tie',
            route: '/admin/usuarios-imobiliaria',
          },
          {
            name: 'Adminstradores',
            icon: 'fas fa-user-shield',
            route: '/admin/usuarios',
          },
        ],
        openGroup: false,
      },
      {
        name: 'Configurações',
        icon: 'fas fa-cogs',
        group: [
          {
            name: 'Geral',
            icon: 'fas fa-cog',
            route: '/admin/configuracao-geral',
          },
          {
            name: 'E-mail',
            icon: ' fas fa-paper-plane',
            route: '/admin/configuracao-email',
          },
          {
            name: 'Contato',
            icon: 'fas fa-phone-alt',
            route: '/admin/contato-geral',
          },
        ],
        openGroup: false,
      },
    ];
  }

  onTipoDataChange(e) {
    if (!e) {
      return;
    }
    // this.getData('APARICOES');
    this.getData('CLICKS');
    this.getData('CONTATOS');
    this.getData('ACESSOAPP');
  }

  onDataFinalChange(e) {
    if (e) {
      // this.getData('APARICOES');
      this.getData('CLICKS');
      this.getData('CONTATOS');
      this.getData('ACESSOAPP');
    }
  }

  onTipoLocalizacaoChange(e) {
    if (!e) {
      return;
    }
    this.getLocationData();
  }
}
