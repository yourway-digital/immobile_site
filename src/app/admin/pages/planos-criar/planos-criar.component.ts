// Default
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { Plano } from 'src/app/models/plano.model';
import { PlanoService } from 'src/app/services/admin/plano.service';

@Component({
  selector: 'app-planos-criar',
  templateUrl: './planos-criar.component.html',
  styleUrls: ['./planos-criar.component.scss']
})
export class PlanosCriarComponent implements OnInit {

  data: Plano = new Plano();

  constructor(
    public planoService: PlanoService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public global: GlobalService
  ) { }

  ngOnInit() {
  }

  submit(f: NgForm) {

    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar('Formulário inválido', 'Preencha os campos requiridos, por favor!', true);
      return;
    }

    this.loadingService.present('Inserindo item...');

    this.planoService.post(this.data)
      .subscribe((res: any) => {
        this.router.navigate(['/admin/planos']).then(() => {
          this.helper.openSnackBar('Inserção', 'Item inserido com sucesso.');
          this.loadingService.dismiss();
        })
      }, e => this.loadingService.dismiss());
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? this.data.situacao = 'A' : this.data.situacao = 'I';
  }

}
