import { UsuarioImobiliaria } from 'src/app/models/usuario-imobiliaria.model';
import {
  UsuarioImobiliariaService,
  UsuariosApi,
} from 'src/app/services/admin/usuario-imobiliaria.service';
// default
import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { DialogComponent } from 'src/app/admin/components/dialog/dialog.component';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { Imobiliaria } from 'src/app/models/imobiliaria.model';
import { ImobiliariaService } from 'src/app/services/admin/imobiliaria.service';

@Component({
  selector: 'app-usuarios-imobiliaria',
  templateUrl: './usuarios-imobiliaria.component.html',
  styleUrls: ['./usuarios-imobiliaria.component.scss'],
})
export class UsuariosImobiliariaComponent implements OnInit {
  displayedColumns: string[] = [
    'select',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    'actions',
  ];
  // nome, email, tipo, situacao
  data: UsuarioImobiliaria[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  search: string;
  filterSubscription: Subscription;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  selection = new SelectionModel<UsuarioImobiliaria>(true, []);

  tipo: string = '%';
  situacao: string = '%';

  idImobiliaria: string = '';
  searchImobiliarias: string = '';
  imobiliarias: Imobiliaria[] = [];

  constructor(
    public global: GlobalService,
    public usuarioImobiliariaService: UsuarioImobiliariaService,
    public imobiliaService: ImobiliariaService,
    public dialog: MatDialog,
    public helper: HelperService,
    public loadingService: LoadingService
  ) {}

  ngOnInit() {}

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    this.buscar();
    this.buscarImobiliarias();
  }

  buscar() {
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.usuarioImobiliariaService.get(
            this.paginator.pageIndex + 1,
            this.paginator.pageSize,
            this.sort.active,
            this.sort.direction.toLocaleUpperCase(),
            this.idImobiliaria ? this.idImobiliaria : '%',
            this.tipo,
            this.situacao,
            this.search
          );
        }),
        map((data) => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.selection.clear();
          this.resultsLength = data.numeroPaginas * this.paginator.pageSize;
          return data.usuarios;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          this.selection.clear();
          return observableOf([]);
        })
      )
      .subscribe((data: any) => {
        this.data = data.map((item) => {
          item.loading = false;
          return item;
        });
      });
  }

  buscarImobiliarias() {
    this.imobiliaService
      .get(-99, -99, '1', 'ASC', 'A')
      .subscribe((res) => (this.imobiliarias = res.imobiliarias));
  }

  onImobiliariaChange(e) {
    if (!e) {
      this.idImobiliaria = '';
    }
    this.buscar();
  }

  deletarData(item: UsuarioImobiliaria) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {
        title: 'Excluir item',
        description:
          'Você realmente quer excluir esse item? Esse processe não pode ser desfeito.',
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.loadingService.present('Excluindo item...');
        this.usuarioImobiliariaService.delete(item).subscribe(
          (res: any) => {
            this.helper.openSnackBar('Remoção', 'Item removido com sucesso.');
            this.loadingService.dismiss();
            this.buscar();
          },
          (e) => this.loadingService.dismiss()
        );
      }
    });
  }

  deletarDatas() {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {
        title: 'Excluir itens',
        description:
          'Você realmente quer excluir esses itens? Esse processe não pode ser desfeito.',
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.usuarioImobiliariaService
          .deleteSelected(this.selection.selected)
          .subscribe(
            (res: any) => {
              this.loadingService.present('Excluindo itens');
              this.helper.openSnackBar(
                'Remoção',
                'Itens removidos com sucesso.'
              );
              this.loadingService.dismiss();
              this.buscar();
            },
            (e) => this.loadingService.dismiss()
          );
      }
    });
  }

  filter(e) {
    if (this.paginator.pageIndex > 1) {
      this.paginator.pageIndex = 0;
    }
    if (e) {
      if (this.filterSubscription && !this.filterSubscription.closed) {
        this.filterSubscription.unsubscribe();
      }
      this.filterSubscription = this.usuarioImobiliariaService
        .get(
          this.paginator.pageIndex + 1,
          this.paginator.pageSize,
          this.sort.active,
          this.sort.direction.toLocaleUpperCase(),
          this.idImobiliaria,
          this.tipo,
          this.situacao,
          e.toLocaleLowerCase()
        )
        .subscribe((res: UsuariosApi) => {
          this.data =
            this.paginator.pageIndex == 0
              ? res.usuarios
              : this.data.concat(res.usuarios);
        });
    } else {
      this.buscar();
    }
  }

  clearFilter() {
    this.search = '';
    this.buscar();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.data.forEach((row) => this.selection.select(row));
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.position + 1
    }`;
  }

  visualizarSenha(usuario: UsuarioImobiliaria) {
    usuario.loading = true;
    this.usuarioImobiliariaService.getPassword(usuario.id).subscribe(
      (password) => {
        usuario.loading = false;
        this.dialog.open(DialogComponent, {
          width: '400px',
          data: {
            title: `${usuario.nome} - Senha`,
            description: atob(password),
          },
        });
      },
      (e) => (usuario.loading = false)
    );
  }
}
