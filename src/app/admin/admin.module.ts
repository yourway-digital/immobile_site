import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// imports
import { AdminRoutingModule } from './admin-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { PipesModule } from '../pipes/pipes.module';
import { NgxSummernoteModule } from 'ngx-summernote';
import { NgxMaskModule } from 'ngx-mask';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgxCurrencyModule } from 'ngx-currency';
import { GalleryModule } from '@ks89/angular-modal-gallery';
// main
import { MainAdminComponent } from './main-admin/main-admin.component';
// components
import { DialogComponent } from './components/dialog/dialog.component';
import { HeaderComponent } from './components/header/header.component';
import { UploadDialogComponent } from './components/upload-dialog/upload-dialog.component';
// pages
import { HomeComponent } from './pages/home/home.component';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';
import { UsuariosCriarComponent } from './pages/usuarios-criar/usuarios-criar.component';
import { UsuariosEditarComponent } from './pages/usuarios-editar/usuarios-editar.component';
import { ContatoGeralComponent } from './pages/contato-geral/contato-geral.component';
import { ConfiguracaoGeralComponent } from './pages/configuracao-geral/configuracao-geral.component';
import { ConfiguracaoEmailComponent } from './pages/configuracao-email/configuracao-email.component';
import { ImobiliariasComponent } from './pages/imobiliarias/imobiliarias.component';
import { ImobiliariasCriarComponent } from './pages/imobiliarias-criar/imobiliarias-criar.component';
import { ImobiliariasEditarComponent } from './pages/imobiliarias-editar/imobiliarias-editar.component';
import { TiposEmpreendimentoComponent } from './pages/tipos-empreendimento/tipos-empreendimento.component';
import { TiposEmpreendimentoCriarComponent } from './pages/tipos-empreendimento-criar/tipos-empreendimento-criar.component';
import { TiposEmpreendimentoEditarComponent } from './pages/tipos-empreendimento-editar/tipos-empreendimento-editar.component';
import { UsuariosImobiliariaComponent } from './pages/usuarios-imobiliaria/usuarios-imobiliaria.component';
import { UsuariosImobiliariaCriarComponent } from './pages/usuarios-imobiliaria-criar/usuarios-imobiliaria-criar.component';
import { UsuariosImobiliariaEditarComponent } from './pages/usuarios-imobiliaria-editar/usuarios-imobiliaria-editar.component';
import { UsuariosAppComponent } from './pages/usuarios-app/usuarios-app.component';
import { UsuariosAppEditarComponent } from './pages/usuarios-app-editar/usuarios-app-editar.component';
import { EmpreendimentosComponent } from './pages/empreendimentos/empreendimentos.component';
import { EmpreendimentosCriarComponent } from './pages/empreendimentos-criar/empreendimentos-criar.component';
import { EmpreendimentosEditarComponent } from './pages/empreendimentos-editar/empreendimentos-editar.component';
import { SubtiposEmpreendimentoComponent } from './pages/subtipos-empreendimento/subtipos-empreendimento.component';
import { SubtipoDialogComponent } from './components/subtipo-dialog/subtipo-dialog.component';
import { MapComponent } from './components/map/map.component';
import { DialogPasswordComponent } from './components/dialog-password/dialog-password.component';
import { PlanosComponent } from './pages/planos/planos.component';
import { PlanosCriarComponent } from './pages/planos-criar/planos-criar.component';
import { PlanosEditarComponent } from './pages/planos-editar/planos-editar.component';
import { CreditsDialogComponent } from './components/credits-dialog/credits-dialog.component';
import { EstatisticasComponent } from './pages/estatisticas/estatisticas.component';
import { EstatisticasUsuarioComponent } from './pages/estatisticas-usuario/estatisticas-usuario.component';
import { AddressDialogComponent } from './components/address-dialog/address-dialog.component';
import { SharedModule } from '../shared/shared.module';
import { NgxDropzoneModule } from 'ngx-dropzone';

@NgModule({
  declarations: [
    // components
    MainAdminComponent,
    DialogComponent,
    HeaderComponent,
    UploadDialogComponent,
    // pages
    HomeComponent,
    UsuariosComponent,
    UsuariosCriarComponent,
    UsuariosEditarComponent,
    ContatoGeralComponent,
    ConfiguracaoGeralComponent,
    ConfiguracaoEmailComponent,
    ImobiliariasComponent,
    ImobiliariasCriarComponent,
    ImobiliariasEditarComponent,
    TiposEmpreendimentoComponent,
    TiposEmpreendimentoCriarComponent,
    TiposEmpreendimentoEditarComponent,
    UsuariosImobiliariaComponent,
    UsuariosImobiliariaCriarComponent,
    UsuariosImobiliariaEditarComponent,
    UsuariosAppComponent,
    UsuariosAppEditarComponent,
    EmpreendimentosComponent,
    EmpreendimentosCriarComponent,
    EmpreendimentosEditarComponent,
    SubtiposEmpreendimentoComponent,
    SubtipoDialogComponent,
    MapComponent,
    DialogPasswordComponent,
    PlanosComponent,
    PlanosCriarComponent,
    PlanosEditarComponent,
    CreditsDialogComponent,
    EstatisticasComponent,
    EstatisticasUsuarioComponent,
    AddressDialogComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    PipesModule,
    NgxSummernoteModule,
    NgxMaskModule.forChild(),
    NgxCurrencyModule,
    NgxMatSelectSearchModule,
    GalleryModule,
    SharedModule,
    NgxDropzoneModule,
  ],
})
export class AdminModule {}
