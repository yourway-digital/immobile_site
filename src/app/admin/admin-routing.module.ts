import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ConfiguracaoEmailComponent } from './pages/configuracao-email/configuracao-email.component';
import { ConfiguracaoGeralComponent } from './pages/configuracao-geral/configuracao-geral.component';
import { ContatoGeralComponent } from './pages/contato-geral/contato-geral.component';
import { UsuariosCriarComponent } from './pages/usuarios-criar/usuarios-criar.component';
import { UsuariosEditarComponent } from './pages/usuarios-editar/usuarios-editar.component';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';
import { ImobiliariasComponent } from './pages/imobiliarias/imobiliarias.component';
import { ImobiliariasCriarComponent } from './pages/imobiliarias-criar/imobiliarias-criar.component';
import { ImobiliariasEditarComponent } from './pages/imobiliarias-editar/imobiliarias-editar.component';
import { TiposEmpreendimentoComponent } from './pages/tipos-empreendimento/tipos-empreendimento.component';
import { TiposEmpreendimentoCriarComponent } from './pages/tipos-empreendimento-criar/tipos-empreendimento-criar.component';
import { TiposEmpreendimentoEditarComponent } from './pages/tipos-empreendimento-editar/tipos-empreendimento-editar.component';
import { UsuariosImobiliariaComponent } from './pages/usuarios-imobiliaria/usuarios-imobiliaria.component';
import { UsuariosImobiliariaCriarComponent } from './pages/usuarios-imobiliaria-criar/usuarios-imobiliaria-criar.component';
import { UsuariosImobiliariaEditarComponent } from './pages/usuarios-imobiliaria-editar/usuarios-imobiliaria-editar.component';
import { UsuariosAppComponent } from './pages/usuarios-app/usuarios-app.component';
import { UsuariosAppEditarComponent } from './pages/usuarios-app-editar/usuarios-app-editar.component';
import { EmpreendimentosComponent } from './pages/empreendimentos/empreendimentos.component';
import { EmpreendimentosCriarComponent } from './pages/empreendimentos-criar/empreendimentos-criar.component';
import { EmpreendimentosEditarComponent } from './pages/empreendimentos-editar/empreendimentos-editar.component';
import { PlanosComponent } from './pages/planos/planos.component';
import { PlanosCriarComponent } from './pages/planos-criar/planos-criar.component';
import { PlanosEditarComponent } from './pages/planos-editar/planos-editar.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'configuracao-email', component: ConfiguracaoEmailComponent },
  { path: 'configuracao-geral', component: ConfiguracaoGeralComponent },
  { path: 'contato-geral', component: ContatoGeralComponent },
  { path: 'usuarios', component: UsuariosComponent },
  { path: 'usuarios/adicionar', component: UsuariosCriarComponent },
  { path: 'usuarios/editar/:id', component: UsuariosEditarComponent },
  { path: 'imobiliarias', component: ImobiliariasComponent },
  { path: 'imobiliarias/adicionar', component: ImobiliariasCriarComponent },
  { path: 'imobiliarias/editar/:id', component: ImobiliariasEditarComponent },
  { path: 'tipos-empreendimento', component: TiposEmpreendimentoComponent },
  { path: 'tipos-empreendimento/adicionar', component: TiposEmpreendimentoCriarComponent },
  { path: 'tipos-empreendimento/editar/:id', component: TiposEmpreendimentoEditarComponent },
  { path: 'usuarios-imobiliaria', component: UsuariosImobiliariaComponent },
  { path: 'usuarios-imobiliaria/adicionar', component: UsuariosImobiliariaCriarComponent },
  { path: 'usuarios-imobiliaria/editar/:id', component: UsuariosImobiliariaEditarComponent },
  { path: 'usuarios-app', component: UsuariosAppComponent },
  { path: 'usuarios-app/editar/:id', component: UsuariosAppEditarComponent },
  { path: 'empreendimentos', component: EmpreendimentosComponent },
  { path: 'empreendimentos/adicionar', component: EmpreendimentosCriarComponent },
  { path: 'empreendimentos/editar/:id', component: EmpreendimentosEditarComponent },
  { path: 'planos', component: PlanosComponent },
  { path: 'planos/adicionar', component: PlanosCriarComponent },
  { path: 'planos/editar/:id', component: PlanosEditarComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
