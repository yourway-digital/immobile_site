import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AuthService, AuthType } from 'src/app/services/auth.service';
import { HelperService } from 'src/app/services/helper.service';
import { UsuarioAdmin } from 'src/app/models/usuario-admin.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  usuario: UsuarioAdmin = new UsuarioAdmin();
  isLogin: boolean = true;
  authType: AuthType = 'gerenciador';
  loading: boolean = false;

  constructor(
    public auth: AuthService,
    public helper: HelperService,
    public router: Router,
    public dialog: MatDialog,
    public route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      const authType = params?.['type'];

      if (!authType) {
        this.router.navigate(['/login', 'admin']);
        return;
      }

      if (authType === 'admin' || authType === 'imobiliaria') {
        this.authType = authType === 'admin' ? 'gerenciador' : 'imobiliaria';
        this.auth.type = authType === 'admin' ? 'gerenciador' : 'imobiliaria';
      } else {
        this.router.navigate(['/login', 'admin']);
      }
    });
  }

  login(f: NgForm) {
    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      return;
    }

    this.loading = true;

    this.auth.login(this.usuario.email, this.usuario.senha).subscribe(
      (res: any) => {
        if (!res.autenticacao.sucesso) {
          this.helper.openSnackBar(
            'Erro no login',
            res.autenticacao.erro,
            true
          );
          this.loading = false;
          return;
        }

        this.auth.setToken(btoa(res.email + ':' + this.usuario.senha));
        this.auth.setUser(res);
        this.auth.setAuthType(this.authType);

        this.loading = false;
        if (this.authType === 'gerenciador') {
          this.router.navigate(['/admin']);
        } else {
          this.router.navigate(['/imobiliaria']);
        }
      },
      (e) => (this.loading = false)
    );
  }

  esqueceuSenha(f: NgForm) {
    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar('Preencha o formulário corretamente');
      return;
    }

    this.loading = true;

    this.auth
      .forgotPassword(this.usuario)
      .then((res: any) => {
        this.helper.openSnackBar(
          'Verifique sua caixa de entrada. E-mail enviado.'
        );
        f.reset();
        this.loading = false;
      })
      .catch((e) => (this.loading = false));
  }

  onChangeAuthType(e) {
    this.auth.type = e;
  }
}
