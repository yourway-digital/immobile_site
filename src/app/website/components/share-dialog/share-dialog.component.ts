import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Empreendimento } from 'src/app/models/empreendimento.model';

@Component({
  selector: 'app-share-dialog',
  templateUrl: './share-dialog.component.html',
  styleUrls: ['./share-dialog.component.scss'],
})
export class ShareDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ShareDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      empreendimento: Empreendimento;
    }
  ) {}

  ngOnInit(): void {}

  shareLink(method: 'facebook' | 'twitter' | 'linkedin' | 'mail' | 'whatsapp') {
    const title = this.data.empreendimento?.nome;
    const url = `https://www.immobile.digital/empreendimento/${this.data.empreendimento?.slug}`;
    const text = `De uma olhada neste empreendimento: ${this.data.empreendimento.nome} no endereço ${this.data.empreendimento.endereco}, ${this.data.empreendimento.numeroEndereco} - Bairro ${this.data.empreendimento.bairro} - ${this.data.empreendimento.cidade} - ${this.data.empreendimento.uf} \n ${url}`;
    const link = {
      facebook: `https://www.facebook.com/sharer/sharer.php?u=${url}/&t=${title}`,
      twitter: `https://twitter.com/intent/tweet?url=${url}&text=${text}`,
      linkedin: `https://www.linkedin.com/shareArticle?mini=1&url=${url}&title=${title}`,
      mail: `mailto:?subject=${title}&body=${text}`,
      whatsapp: `https://wa.me/?text=${text}`,
    };

    window.open(link[method], '_blank');
  }
}
