import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { Empreendimento } from 'src/app/models/empreendimento.model';
import { EmpreendimentoService } from 'src/app/services/app/empreendimento.service';

@Component({
  selector: 'app-contact-dialog',
  templateUrl: './contact-dialog.component.html',
  styleUrls: ['./contact-dialog.component.scss'],
})
export class ContactDialogComponent implements OnInit {
  contacts: any[] = [];

  constructor(
    public dialogRef: MatDialogRef<ContactDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      empreendimento: Empreendimento;
    },
    private empreendimentoService: EmpreendimentoService,
    private title: Title
  ) {}

  ngOnInit(): void {
    this.getContacts();
  }

  getContacts() {
    this.empreendimentoService
      .getContacts(this.data.empreendimento.idEndereco)
      .subscribe((res) => {
        console.log(res);
        this.contacts = res;
      });
  }

  openWhatsApp() {
    try {
      // await EmpreendimentoEstatisticaService.insert(
      //   this.data.empreendimento.id,
      //   user ? user?.id : 0
      // );
      const phoneNumber =
        this.data.empreendimento.entrarEmContato === 'I'
          ? this.data.empreendimento.imobiliaria?.whatsapp
          : this.data.empreendimento.usuarioImobiliaria?.whatsapp;
      const message = `Olá, estou interessado no imóvel ${this.data.empreendimento.nome} no endereço ${this.data.empreendimento.endereco}, ${this.data.empreendimento.numeroEndereco} ${this.data.empreendimento.cep} Bairro ${this.data.empreendimento.bairro} - ${this.data.empreendimento.cidade} - ${this.data.empreendimento.uf}`;
      window.open(`https://wa.me/55${phoneNumber}?text=${message}`, '_blank');
    } catch (error) {}
  }

  makeCall(contato: Empreendimento) {
    const phoneNumber =
      contato?.entrarEmContato === 'I'
        ? contato?.imobiliaria?.telefone
        : contato?.usuarioImobiliaria?.whatsapp;
    window.open(`tel:0${phoneNumber}`, '_target');
  }
}
