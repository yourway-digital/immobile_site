import { Platform } from '@angular/cdk/platform';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmpreendimentoImagem } from 'src/app/models/empreendimento-imagem.model';
import { Empreendimento } from 'src/app/models/empreendimento.model';
import { EmpreendimentoService } from 'src/app/services/app/empreendimento.service';
import { GlobalService } from 'src/app/services/global.service';
import { Fancybox, Carousel, Panzoom } from '@fancyapps/ui';
import { MatDialog } from '@angular/material/dialog';
import { ContactDialogComponent } from '../../components/contact-dialog/contact-dialog.component';
import { Meta, Title } from '@angular/platform-browser';
import { HelperService, MetaTags } from 'src/app/services/helper.service';
import { ShareDialogComponent } from '../../components/share-dialog/share-dialog.component';
import { PixelService } from 'ngx-pixel';

@Component({
  selector: 'app-propertie-detail',
  templateUrl: './propertie-detail.component.html',
  styleUrls: ['./propertie-detail.component.scss'],
})
export class PropertieDetailComponent implements OnInit {
  isLoading = true;
  empreendimento: Empreendimento = new Empreendimento();

  gallery = {
    main: new EmpreendimentoImagem(),
    grid: [],
    total: [],
  };
  infos: any[] = [];

  constructor(
    public route: ActivatedRoute,
    public platform: Platform,
    public empreendimentoService: EmpreendimentoService,
    public global: GlobalService,
    private dialog: MatDialog,
    private title: Title,
    private helper: HelperService,
    private pixelService: PixelService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params['id']) {
        this.getEmpreendimento(params['id']);
      }
    });

    this.pixelService.track('ViewContent');
  }

  getEmpreendimento(id: number) {
    this.isLoading = true;
    this.empreendimentoService
      .getById({
        id,
      })
      .subscribe({
        next: (res) => {
          console.log(res);
          this.title.setTitle(`${res.nome} - Empreendimento - Immobile`);
          this.helper.updateTags({
            nome: `${res.nome} - Empreendimento - Immobile`,
            imagem: this.global.imageUrl + res.imagens[0].imagem,
            descricaoSimples: `${res.cep} -
            ${res.bairro} - ${res.endereco}
            ${res.enderecoNumero}
            ${res.complemento} - ${res.cidade} -
            ${res.uf} `,
          });

          this.empreendimento = res;
          if (res.imagens.length) {
            this.gallery.main = res.imagens[0];
            this.gallery.grid = res.imagens.filter(
              (_, index) => index > 0 && index < 5
            );
            this.gallery.total = res.imagens;
          }
          this.infos = [
            {
              label: 'Tipo de Imóvel',
              value: res.tipoEmpreendimento.nome,
              visible: true,
            },
            {
              label: 'Subtipo de Imóvel',
              value: res.subtipoEmpreendimento.nome,
              visible: true,
            },
            {
              label: 'Tipo de Comodite',
              value: this.global.getEmpreendimentoTipoOfertaLabel(
                res.tipoOferta
              ),
              visible: true,
            },
            {
              label: 'Nº quartos',
              value: res.numeroQuartos,
              visible: res.numeroQuartos > 0,
            },
            {
              label: 'Nº banheiros',
              value: res.numeroBanheiros,
              visible: res.numeroBanheiros > 0,
            },
            {
              label: 'Nº suítes',
              value: res.numeroSuites,
              visible: res.numeroSuites > 0,
            },
            {
              label: 'Nº box de garagem',
              value: res.numeroBoxGaragem,
              visible: res.numeroBoxGaragem > 0,
            },
            {
              label: 'Dimensão terreno',
              value: res.tamanhoTerrenoM2.toFixed(2) + ' m²',
              visible: res.tamanhoTerrenoM2 > 0,
            },
            {
              label: 'Dimensão da área construída',
              value: res.tamanhoAreaConstruidaM2.toFixed(2) + ' m²',
              visible: res.tamanhoAreaConstruidaM2 > 0,
            },
          ];
        },
        complete: () => {
          setTimeout(() => {
            this.isLoading = false;
          }, 500);
        },
      });
  }

  openFancyBox(index: number = 0) {
    Fancybox.show(
      this.gallery.total.map((item) => ({
        src: this.global.imageUrl + item?.imagem,
        thumb: this.global.imageUrl + item?.imagem,
      })),
      {
        startIndex: index,
      }
    );
  }

  openContactDialog() {
    this.pixelService.track('Lead');

    const dialogRef = this.dialog.open(ContactDialogComponent, {
      width: '420px',
      data: {
        empreendimento: this.empreendimento,
      },
    });

    dialogRef.afterClosed().subscribe((res) => {
      console.log(res);
    });
  }

  openSharedDialog() {
    const dialogRef = this.dialog.open(ShareDialogComponent, {
      width: '420px',
      data: {
        empreendimento: this.empreendimento,
      },
    });

    dialogRef.afterClosed().subscribe((res) => {
      console.log(res);
    });
  }
}
