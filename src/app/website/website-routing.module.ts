import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { PoliticaDePrivacidadeComponent } from './pages/politica-de-privacidade/politica-de-privacidade.component';
import { PropertieDetailComponent } from './pages/propertie-detail/propertie-detail.component';
import { TermosDeUsoComponent } from './pages/termos-de-uso/termos-de-uso.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'empreendimento/:id', component: PropertieDetailComponent },
  { path: 'termos-de-uso', component: TermosDeUsoComponent },
  {
    path: 'politica-de-privacidade',
    component: PoliticaDePrivacidadeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WebsiteRoutingModule {}
