import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WebsiteRoutingModule } from './website-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { FormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { MainComponent } from './main/main.component';
import { TermosDeUsoComponent } from './pages/termos-de-uso/termos-de-uso.component';
import { PoliticaDePrivacidadeComponent } from './pages/politica-de-privacidade/politica-de-privacidade.component';
import { NgxPageScrollModule } from 'ngx-page-scroll';
import { NgxPageScrollCoreModule } from 'ngx-page-scroll-core';
import { PropertieDetailComponent } from './pages/propertie-detail/propertie-detail.component';
import { MaterialModule } from '../material/material.module';
import { ContactDialogComponent } from './components/contact-dialog/contact-dialog.component';
import { ShareDialogComponent } from './components/share-dialog/share-dialog.component';

@NgModule({
  declarations: [
    HomeComponent,
    MainComponent,
    TermosDeUsoComponent,
    PoliticaDePrivacidadeComponent,
    PropertieDetailComponent,
    ContactDialogComponent,
    ShareDialogComponent,
  ],
  imports: [
    CommonModule,
    WebsiteRoutingModule,
    FormsModule,
    NgxMaskModule.forChild(),
    NgxPageScrollCoreModule,
    NgxPageScrollModule,
    MaterialModule,
  ],
})
export class WebsiteModule {}
